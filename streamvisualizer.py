from itertools import product

print("You care for the land and water it; you enrich it abundantly. The streams of God are filled with water to provide the people with grain, for so you have ordained it. (Psalms 65, 9)\n\n")

TRANSPARENT = ""

def spaceToTransparent(char):
  return TRANSPARENT if char == " " else char

def createEmpty():
  """
    Creates an empty StringArt
  """
  return lambda coord: TRANSPARENT

def create(string):
  """
    Creates a StringArt from a string
  """
  lines = string.split("\n")
  def result(coord):
    x, y = coord
    if x >= 0  and x < len(lines):
      if y >= 0 and y < len(lines[x]):
        return spaceToTransparent(lines[x][y])
    return TRANSPARENT  
  return result
    
def translate(stringArt, vect):
  """
    Translates a StringArt to a different position
  """
  return lambda coord: stringArt((coord[0] - vect[0], coord[1] - vect[1]))
  
def compose(stringArt, func):
  """
    Applies a string->string function to a stringArt.
  """
  return lambda coord: func(stringArt(coord))

def union(*stringArts):
  """
    Takes a list of StringArts and returns a stringArt that
    returns the first non-transparent value
  """
  def result(coord):
    for art in stringArts:
      if art(coord) != TRANSPARENT:
        return art(coord)
    return TRANSPARENT
  return result

def createHoledFrame(size, border = "#"):
    height, width = size
    def result(coord):
        x, y = coord
        if x < 0 or y < 0 or x >= height or y >= width:
            return TRANSPARENT
        if (y == 0 or y == y == width -1) and 2 * x + 1 == height:
            return TRANSPARENT
        if x == 0 or y == 0 or x == height - 1 or y == width -1:
            return border
        return TRANSPARENT
    return result

#---------------------------
# sizedArts are dictionaries that contain a size and a
# stringArt.

def createSizedArt(size, art):
  """
    Creates a sized art
  """
  return {"size": size, "art": art}

def getArt(sizedArt):
  """
    Returns the art of a sizedArt
  """
  return sizedArt["art"]

def getSize(sizedArt):
  """
    Returns the size of a SizedArt
  """
  return sizedArt["size"]

def getWidth(sizedArt):
  """
    Returns the width of a sizedArt
  """
  return getSize(sizedArt)[1]

def getHeight(sizedArt):
  """
    Returns the height of a sizedArt
  """
  return getSize(sizedArt)[0]

def resize(sizedArt, size):
  """
    Resizes a sized art
  """
  return createSizedArt(size, getArt(sizedArt))

def createEmptySizedArt():
  """
    Creates an empty sized art
  """
  return createSizedArt((0, 0), createEmpty())

def addArtsToSizedArt(sizedArt, *arts):
  mainArt = getArt(sizedArt)
  size = getSize(sizedArt)
  return createSizedArt(size, union(mainArt, *arts))
    

#-------------------------------------
# Methods that are responsible for displaying stringArts and sizedArts

def resolveChar(char):
  """
    Resolves a character from a StringArt into a printable format
  """
  return " " if char == TRANSPARENT else char

def printArt(stringArt, dimension):
  """
    Prints a StringArt in a rectangle of sizeX x sizeY
  """
  sizeX, sizeY = dimension
  for i in range(sizeX):
    line = "".join([resolveChar(stringArt((i, j))) for j in range(sizeY)])
    print(line)

def printSizedArt(sizedArt):
  """
    Prints a sized art.
  """
  printArt(getArt(sizedArt), getSize(sizedArt))

## ------------------------------------------

def appendSizedArts(*sizedArts):
    width = sum(getWidth(art) for art in sizedArts)
    height = max(getHeight(art) for art in sizedArts)
    translateY = 0
    arts = []
    for sizedArt in sizedArts:
      art = getArt(sizedArt)
      arts.append(translate(art, (0, translateY)))
      translateY += getWidth(sizedArt)
    return createSizedArt((height, width), union(*arts))

def getStrFromDict(dictionary, key):
    result = dictionary.get(key, "")
    return str(result) if result != None else ""

def createPipePartArt(**kwargs):
    name = getStrFromDict(kwargs, "name")
    top = getStrFromDict(kwargs, "top")
    bottom = getStrFromDict(kwargs, "bottom")
    middle = getStrFromDict(kwargs, "middle")
    output = getStrFromDict(kwargs, "output")
    height = 6
    frameWidth = max(len(top) + 2, len(bottom) + 2, len(middle) + 5, len(name), 5)
    nameArt = translate(create(name.center(frameWidth)), (5, 0))
    topArt = translate(create(top), (1, 1))
    bottomArt = translate(create(bottom), (3, 1))
    middleArt = translate(create(middle), (2, 2))
    frameArt = createHoledFrame((5, frameWidth))
    connectorArt = createConnectorArt(output)
    mainSizedArt = createSizedArt((height, frameWidth),\
              union(nameArt, topArt, bottomArt, middleArt, frameArt))
    return appendSizedArts(mainSizedArt, connectorArt)
    

def createConnectorArt(item):
    if item == None:
        return createConnectorArt(" ")
    string = str(item)
    width = len(string) + 1
    height = 4
    oneBorder = create("#" * width)
    borderArt = union(translate(oneBorder, (1, 0)), translate(oneBorder, (3, 0)))
    insideArt = translate(create(string), (2, 0))
    return createSizedArt((height, width), union(borderArt, insideArt))

class Stream:
    @staticmethod
    def of(*items):
        return SourceStream(*items)

class StreamPart:

    def __init__(self):
        self.output = None

    def getSizedArt(self):
        pass
    
    def step(self):
        pass

    def takeOutput(self):
        result = self.output
        self.output = None
        return result

    def hasOutput(self):
        return self.output != None

    def isFinished(self):
        return True

    def filter(self, predicate, name = "Filter"):
        return FilterStream(self, predicate, name)

    def map(self, function, name = "Map"):
        return MapStream(self, function, name)

    def limit(self, limit, name = "Limit"):
        return LimitStream(self, limit, name)

    def reduce(self, initValue, biFunction, name = "Reduce"):
        return Reduce(self, initValue, biFunction, name).doReduction()

    def skip(self, skipTotal, name = "Skip"):
        return SkipStream(self, skipTotal, name)
    
class SourceStream(StreamPart):

    def __init__(self, *items):
        self.name = "Source"
        self.output = None
        self.items = items
        self.originalWidth = self.getWidth()
        
    def step(self):
        if len(self.items) > 0:
            self.output = self.items[0]
            self.items = self.items[1:]

    def getVisibleItemsStr(self):
        return ", ".join(map(str, self.items[0:3][::-1]))

    def getWidth(self):
        return len(self.getVisibleItemsStr())

    def getSizedArt(self):
        itemsStr = self.getVisibleItemsStr()
        mainArt = createPipePartArt(name = self.name, middle = itemsStr,\
                                 output = self.output)
        return addArtsToSizedArt(mainArt, create("\n\n#"))
      
    def isFinished(self):
        return len(self.items) == 0 and self.output == None

class MapStream(StreamPart):

    def __init__(self, sourceStream, mappingFunc, name = "Map"):
        self.name = name
        self.output = None
        self.sourceStream = sourceStream
        self.mappingFunc = mappingFunc

    def step(self):
        if self.sourceStream.hasOutput():
            self.output = self.mappingFunc(self.sourceStream.takeOutput())
        else:
            self.sourceStream.step()
            
    def getSizedArt(self):
        ownSizedArt = createPipePartArt(name = self.name, output = self.output)
        return appendSizedArts(self.sourceStream.getSizedArt(), ownSizedArt)
        
    def isFinished(self):
        return self.output == None and self.sourceStream.isFinished()

class FilterStream(StreamPart):
    
    def __init__(self, sourceStream, predicate, name = "Filter"):
        self.name = name
        self.output = None
        self.sourceStream = sourceStream
        self.predicate = predicate
        self.filteredItems = []

    def getLastFiltered(self):
        return "" if len(self.filteredItems) == 0 else self.filteredItems[-1]

    def step(self):
        if self.sourceStream.hasOutput():
            item = self.sourceStream.takeOutput()
            if self.predicate(item):
                self.output = item
            else:
                self.filteredItems.append(item)
        else:
            self.sourceStream.step()

    def getSizedArt(self):
        ownSizedArt = createPipePartArt(name = self.name, bottom = self.getLastFiltered(),\
                                  output = self.output)
        return appendSizedArts(self.sourceStream.getSizedArt(), ownSizedArt)

    def isFinished(self):
        return self.output == None and self.sourceStream.isFinished()

class LimitStream(StreamPart):

    def __init__(self, sourceStream, limit, name = "Limit"):
        self.name = name
        self.output = None
        self.limit = limit
        self.passed = 0
        self.sourceStream = sourceStream
        self.filteredItems = []

    def step(self):
        if self.isClosed():
            return
        if self.sourceStream.hasOutput():
            self.output = self.sourceStream.takeOutput()
            self.passed += 1
        else:
            self.sourceStream.step()

    def getLimitString(self):
        return str(self.passed) + "/" + str(self.limit)

    def isClosed(self):
        return self.passed >= self.limit

    def getSizedArt(self):
      ownSizedArt = createPipePartArt(name = self.name, top = self.getLimitString(),\
                                  output = self.output)
      gateArt = create("\n\n#") if self.isClosed() else createEmpty()
      ownSizedArt = addArtsToSizedArt(ownSizedArt, gateArt)
      return appendSizedArts(self.sourceStream.getSizedArt(), ownSizedArt)

    def isFinished(self):
        return self.output == None and (self.sourceStream.isFinished()\
               or self.isClosed())

class SkipStream(StreamPart):

    def __init__(self, sourceStream, skipTotal, name = "Skip"):
        self.name = name
        self.output = None
        self.skipTotal = skipTotal
        self.skipped = 0
        self.sourceStream = sourceStream
        self.skippedItems = []

    def step(self):
        if self.sourceStream.hasOutput():
          item = self.sourceStream.takeOutput()
          if self.isClosed():
            self.skipped += 1
            self.skippedItems.append(item)
          else:
            self.output = item
        else:
          self.sourceStream.step()

    def isClosed(self):
        return self.skipped < self.skipTotal

    def getSkippedString(self):
        return str(self.skipped) + "/" + str(self.skipTotal)

    def getLastSkipped(self):
        return "" if len(self.skippedItems) == 0 else self.skippedItems[-1]

    def isFinished(self):
        return self.output == None and self.sourceStream.isFinished()

    def getSizedArt(self):
        output = "|" if self.isClosed() else self.output
        ownSizedArt = createPipePartArt(name = self.name, bottom = self.getLastSkipped(),\
                                  output = output, top = self.getSkippedString())
        return appendSizedArts(self.sourceStream.getSizedArt(), ownSizedArt)

class Reduce:
    def __init__(self, sourceStream, initValue, biFunction, name = "Reduce"):
        self.name = name
        self.output = None
        self.sourceStream = sourceStream
        self.currentValue = initValue
        self.biFunction = biFunction

    def step(self):
        if self.sourceStream.isFinished():
            self.output = self.currentValue
            self.currentValue = None
            return
            
        if self.sourceStream.hasOutput():
            item = self.sourceStream.takeOutput()
            self.currentValue = self.biFunction(self.currentValue, item)
        else:
            self.sourceStream.step()

    def isFinished(self):
        return self.output != None

    def getSizedArt(self):
        ownSizedArt = createPipePartArt(name = self.name,\
                                  output = self.output, top = self.currentValue)
        return appendSizedArts(self.sourceStream.getSizedArt(), ownSizedArt)

    def printStatus(self):
        printSizedArt(self.getSizedArt())
        print("\n")
        
    def doReduction(self):
        self.printStatus()
        while not self.isFinished():
            self.step()
            self.printStatus()
        return self.output
    

print("This is how you can calculate the sum of the")
print("first 2 odd square numbers using Java 8 Streams:")

result = Stream.of(1, 2, 3)\
                .map(lambda x: x**2, "x -> x*x")\
                .filter(lambda x: x % 2 == 1, "is odd?")\
                .reduce(0, lambda x, y: x + y, "Sum")

print("The result of the computation is " + str(result))
print("Explanation:")
print("This is a pipe that processes elements")
print("that pass through it (from left to right).")
print("It has 4 parts, each does its own job:")
print("1: The Source provides the numbers for the calculations.\n")
print("2: The Filter allows only some elements to pass through")
print("(in the example if lets odd values to pass).\n")
print("3: The Map transforms the elements (takes their squares).\n")
print("4: The Reduce step consumes an element and")
print("provides the final result (in the example it sums the elements).\n")
