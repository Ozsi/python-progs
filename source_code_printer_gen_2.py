q = chr(34)
tq = q * 3
esc_q = chr(92) + q
esc = chr(92)
nl = chr(10)
esc_nl = esc + "n"
def surround(s, q):
    return q + s + q
def do_format(s):
    return surround(s.replace(nl, esc_nl).replace(q, esc_q), q)
javaSource = """
class Program {
  static String pythonSource = %s;
  public static void main(String[] args) {
    System.out.print(pythonSource);
  }
}"""
pythonSource = """
q = chr(34)
tq = q * 3
esc_q = chr(92) + q
esc = chr(92)
nl = chr(10)
esc_nl = esc + "n"
def surround(s, q):
    return q + s + q
def do_format(s):
    return surround(s.replace(nl, esc_nl).replace(q, esc_q), q)
javaSource = %s
pythonSource = %s
print(javaSource %% do_format(pythonSource %% (surround(javaSource, tq), surround(pythonSource, tq))))"""
print(javaSource % do_format(pythonSource % (surround(javaSource, tq), surround(pythonSource, tq))))
