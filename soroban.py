import random

TRANSPARENT = ""

def lte(x, y):
    return x <= y

def lt(x, y):
    return x < y

class StringArt:
    def __init__(self, art, size):
        self.size = size
        self.art = art

    def isInside(self, coord):
        return all(map(lte, (0, 0), coord)) and all(map(lt, coord, self.size))
    
    def getCharAt(self, coord):
        return self.art(coord) if self.isInside(coord) else TRANSPARENT

    def getDisplayChar(self, coord):
        char = self.getCharAt(coord)
        return char if char != TRANSPARENT else " "

    def getSize(self):
        return self.size

    def __add__(self, other):
        return self.add(other)

    def __rshift__(self, vector):
        return self.translate(vector)

    def add(self, other):
        def art(coord):
            myChar = self.getCharAt(coord)
            otherChar = other.getCharAt(coord)
            return myChar if myChar != "" else otherChar
        size = tuple(map(max, self.getSize(), other.getSize()))
        return StringArt(art, size)

    def translate(self, vector):
        def art(coord):
            x, y = coord
            vx, vy = vector
            return self.art((x - vx, y - vy))
        return StringArt(art, self.size)

    def __xor__(self, newSize):
        return self.resize(newSize)

    def resize(self, newSize):
        return StringArt(self.art, newSize)
    
    def transpose(self):
        def art(coord):
            x, y = coord
            return self.art((y, x))
        width, height = self.size
        return StringArt(art, (height, width))

    def frame(self, vect):
        def art(coord):
            inside = all(map(lte, (0, 0), coord)) and all(map(lt, coord, vect))
            return self.art(coord) if inside else TRANSPARENT
        return StringArt(art, self.size)

    def __or__(self, vect):
        return self.frame(vect)
    
    def __invert__(self):
        return self.transpose()
    
    def print(self):
        width, height = self.size
        for y in range(height):
            line = "".join(self.getDisplayChar((x, y)) for x in range(width))
            print(line)

def createEmpty():
  return lambda coord: TRANSPARENT

def translate(stringArt, vect):
  return lambda coord: stringArt((coord[0] - vect[0], coord[1] - vect[1]))

def frame(art, dimension):
  sizeX, sizeY = dimension
  xRange = range(sizeX)
  yRange = range(sizeY)
  xInside = range(1, sizeX - 1)
  yInside = range(1, sizeY - 1)
  translatedArt = translate(art, (1, 1))
  def result(coord):
    x, y = coord
    if (x not in xRange) or (y not in yRange):
      return TRANSPARENT
    if (x not in xInside) and (y not in yInside):
      return "+"
    if x not in xInside:
      return "|"
    if y not in yInside:
      return "-"
    return translatedArt(coord)
  return result

def getSorobanStickArt(emptySlots):
    def result(coord):
        x, y = coord
        if x % 2 == 0:
            return TRANSPARENT
        else:
            column = x // 2
            return "|" if emptySlots[column] == y else "O"
    return result

def getUpperFrameWithDigits(digits):
    sorobanWidth = getSorobanWidth(len(digits))
    frameSize = (sorobanWidth, 4)
    sorobanSize = (sorobanWidth, 10)
    emptySlots = tuple(map(lambda x: 1 - x, digits))
    return StringArt(frame(getSorobanStickArt(emptySlots), frameSize), sorobanSize)

def getLowerFrameWithDigits(digits):
    sorobanWidth = getSorobanWidth(len(digits))
    frameSize = (sorobanWidth, 7)
    sorobanSize = (sorobanWidth, 10)
    emptySlots = digits
    return StringArt(frame(getSorobanStickArt(emptySlots), frameSize), sorobanSize) >> (0, 3)

def getDigits(displayedNumber, columns):
    upperDigits = []
    lowerDigits = []
    while displayedNumber > 0:
        lowerDigits.append(displayedNumber % 5)
        displayedNumber //= 5
        upperDigits.append(displayedNumber % 2)
        displayedNumber //= 2
    while len(lowerDigits) < columns:
        lowerDigits.append(0)
        upperDigits.append(0)
    return tuple(lowerDigits[::-1]), tuple(upperDigits[::-1])

def getLabelArt(columns, label):
    if label == "":
        return None
    width = getSorobanWidth(columns)
    height = 11
    validIndeces = set(range(len(label)) )
    def art(coord):
        x, y = coord
        if x % 2 == 0 and y == 10:
            index = x // 2 - columns + len(label) - 1
        else:
            index = -1
        return label[index] if index in validIndeces else TRANSPARENT
    return StringArt(art, (width, height))

def getSorobanArt(columns, digits, label = ""):
    sorobanWidth = getSorobanWidth(columns)
    upperFrameSize = (sorobanWidth, 4)
    lowerFrameSize = (sorobanWidth, 7)
    lowerDigits, upperDigits = digits
    upperFrame = getUpperFrameWithDigits(upperDigits)
    lowerFrame = getLowerFrameWithDigits(lowerDigits)
    labelArt = getLabelArt(columns, label)
    mainArt = upperFrame + lowerFrame
    return mainArt if labelArt == None else labelArt + mainArt 

def getSorobanWidth(columns):
    return 2*columns + 3

columns = 13
displayedNumber = random.randrange(1000000)

digits = getDigits(displayedNumber, columns)
getSorobanArt(columns, digits, str(displayedNumber)).print()

def getValidSteps(steps):
    return tuple(filter(lambda x: len(x) > 0, steps.split("\n")))

commandDict = {
    # TODO
  "set": 0,
  "add": 0
}

def parseStep(step):
    parts = step.split(" ")
    command, param, *other = parts
    


steps = """
set 127
add 240
"""

