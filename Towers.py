# Towers of Hanoi

# The number of stones on the tower. Increasing the size BY ONE
# approximately DOUBLES RUNTIME. So be careful :)
SIZE = 3

# These are needed so that the images will be pretty
def getGap():
  return 2

def getStoneWidth(size):
  return 2 * size + 3

def getTowerWidth(size):
  return 2 * size + 3

def getTowerHeight(size):
  return size + 2

def getTowerPosition(size, i):
  return i * (getTowerWidth(size) + getGap())

def getTowerCenter(size, i):
  return getTowerWidth(size) // 2 + getTowerPosition(size, i)

def getPictureHeight(size):
  return getTowerHeight(size) + 3

def getPictureWidth(size):
  return getTowerWidth(size) * 3 + 2 * getGap()

#---------------------------------

def getMoves(start, end, level):
  """
    Yields the moves to move level number of stones
    from start to end 
  """
  if level == 1:
    yield (start, end)
  else:
    midGoal = 3 - start - end
    yield from getMoves(start, midGoal, level - 1)
    yield (start, end)
    yield from getMoves(midGoal, end, level - 1)

def doMove(move, stones):
  """
    Does the actual move
  """
  start, end = move
  stones[end].append(stones[start].pop())
  
#-------------------------------------
# From now on we only need to deal with printing the steps nicely
#
# A StringArt is a function that takes two parameters (x, y)
# and returns a character (or TRANSPARENT)
# that should be printed at that position.
# x represents the row, y represents the column
TRANSPARENT = ""

def emptyArt():
  """
    Creates an empty StringArt
  """
  return lambda x, y: TRANSPARENT

def createStringArt(string):
  """
    Creates a StringArt from a string which takes up 1 row (line)
  """
  def result(x, y):
    if x == 0 and y >= 0 and y < len(string):
      return string[y]
    else:
      return TRANSPARENT
  return result
  
def translateStringArt(stringArt, i, j):
  """
    Translates a StringArt to a different position
  """
  return lambda x, y: stringArt(x - i, y - j)

def transposeStringArt(stringArt):
  """
     Transposes a StringArt by switching x and y coordinates
  """
  return lambda x, y: stringArt(y, x)

def stringArtUnion(stringArts):
  """
    Takes a list of StringArts and returns a stringArt that
    returns the first non-transparent value
  """
  def result(x, y):
    for art in stringArts:
      if art(x, y) != TRANSPARENT:
        return art(x, y)
    return TRANSPARENT
  return result

#-------------------------------------
# Methods that are responsible for displaying StringArts

def resolveChar(char):
  """
    Resolves a character from a StringArt into a printable format
  """
  return  " " if char == TRANSPARENT else char

def printStringArt(stringArt, sizeX, sizeY):
  """
    Prints a StringArt in a rectangle of sizeX x sizeY
  """
  for i in range(sizeX):
    line = "".join([resolveChar(stringArt(i, j)) for j in range(sizeY)])
    print(line) 

#---------------------------------
# From now on we deal with the items we actually want to display

def createTowerBaseAndStick(size):
  """
     This is the base and the stick of the tower, looks like this:
       |
       |
     =====
     A tower of size 1
  """
  height = getTowerHeight(size)
  width = getTowerWidth(size)
  stick = transposeStringArt(createStringArt("|" * height))
  stick = translateStringArt(stick, 0, width // 2)
  base = createStringArt("=" * width)
  base = translateStringArt(base, height - 1, 0)
  return stringArtUnion([base, stick])

def createArrow(start, end):
  """
   Creates an arrow which shows the next move:
     -----v
  """
  leftEnd = min([start, end])
  rightEnd = max([start, end])
  v = createStringArt("v")
  v = translateStringArt(v, 0, end)
  line = createStringArt("-" * (rightEnd - leftEnd + 1))
  line = translateStringArt(line, 0, leftEnd)
  return stringArtUnion([v, line])
  
def createStone(size, center):
  """
     This is a StringArt representing a stone on the tower:
       ooooo
     This is a stone of size 2
     The middle part will be behind a stick
  """
  stoneString = "o" * getStoneWidth(size)
  return createStringArt(stoneString.center(2 * center + 1))

def createTowerWithStones(size, stoneList):
  """
    Creates a StringArt for a tower, including the stones
    that are present on the tower.
  """
  towerBaseArt = createTowerBaseAndStick(size)
  stoneArts = []
  stoneHeight = size
  center = size + 1
  for stoneSize in stoneList:
    stoneArt = createStone(stoneSize, center)
    stoneArts.append(translateStringArt(stoneArt, stoneHeight, 0))
    stoneHeight -= 1
  return stringArtUnion([towerBaseArt] + stoneArts)

def createArtForTower(size, stoneList, i):
  """
    Creates the art for tower #i, and translates it to the required position
  """
  position = getTowerPosition(size, i)
  towerArt = createTowerWithStones(size, stoneList)
  return translateStringArt(towerArt, 2, position)

def createArrowForStep(size, nextStep):
  """
    Creates a StringArt for the arrow
  """
  if nextStep != None:
    startIndex, endIndex = nextStep
    start = getTowerCenter(size, startIndex)
    end = getTowerCenter(size, endIndex)
    result = createArrow(start, end)
  else:
    result = emptyArt()
  return result

def createFullArt(size, stones, nextStep):
  """
     Creates a StringArt of a step, including the arrow
  """
  arrow = createArrowForStep(size, nextStep)
  towerArts = [createArtForTower(size, stones[i], i) for i in range(3)]
  towersArt = stringArtUnion(towerArts)
  return stringArtUnion([arrow, towersArt]) 

def printSteps(size):
  """
    Prints all the steps of the solution
  """
  stones = [list(range(SIZE))[::-1], [], []]
  pictureHeight = getPictureHeight(size)
  pictureWidth = getPictureWidth(size)
  for move in getMoves(0, 2, size):
    print("Move from {0} to {1}".format(*move))
    printStringArt(createFullArt(size, stones, move), pictureHeight, pictureWidth)
    doMove(move, stones)
  print("And we're ready:")
  printStringArt(createFullArt(size, stones, None), pictureHeight, pictureWidth)  

print("\"But do not forget this one thing, dear friends: With the Lord a day is like a thousand years, and a thousand years are like a day.\" (2 Peter 3, 8)\n")
printSteps(SIZE)
