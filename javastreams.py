from itertools import count

# An implementation of some java-style Stream methods

print("You care for the land and water it; you enrich it abundantly. The streams of God are filled with water to provide the people with grain, for so you have ordained it. (Psalms 65, 9)")

# Set this to true if you want the flow to be printed
DEBUG = False


def decor(string):
    def decorated(f):
        def printCalls(self):
            if DEBUG:
                print(string + " is being requested")
            result = f(self)
            if DEBUG:
                print(string + " supplies " + str(result))
            return result
        return printCalls
    return decorated

class StreamPiece:
    def __init__(self):
        pass
    
    def supplyItem(self):
        return None

    def filter(self, predicate):
        return StreamFilter(predicate, self)

    def map(self, func):
        return StreamMap(func, self)

    def limit(self, limit):
        return StreamLimit(limit, self)

    def skip(self, skip):
        return StreamSkip(skip, self)

    def reduce(self, startValue, biFunc):
        return Reducer(self, startValue, biFunc).reduce()

    def collect(self, collector):
        return Reducer(self, collector.startValueSupplier(), collector.merger).mutableReduce()


class Stream:
    @staticmethod
    def of(*items):
        return StreamStart(items)

    @staticmethod
    def concat(*streams):
        return StreamConcat(streams)

class StreamStart(StreamPiece):
    def __init__(self, iterable):
        self.iterable = iter(iterable)

    @decor("Start")
    def supplyItem(self):
        try:
            return self.iterable.__next__()
        except StopIteration:
            return None

class StreamConcat(StreamPiece):
    def __init__(self, streams):
        self.streams = streams
        self.iterable = iter(streams)
        self.nextStream()

    @decor("Concat")
    def supplyItem(self):
        current = self.getCurrentStream()
        if current == None:
            return None
        item = current.supplyItem()
        while item == None and current != None:
            current = self.nextStream()
            item = current.supplyItem() if current != None else None
        if item != None:
            return item
        return None

    def getCurrentStream(self):
        return self.currentStream

    def nextStream(self):
        self.currentStream = self.getNextOrNone(self.iterable)
        return self.currentStream

    def getNextOrNone(self, iterable):
        try:
            return iterable.__next__()
        except StopIteration:
            return None

class StreamFilter(StreamPiece):
    def __init__(self, predicate, supplyStream):
        self.predicate = predicate
        self.supplyStream = supplyStream

    @decor("Filter")
    def supplyItem(self):
        while True:
            item = self.supplyStream.supplyItem()
            if item == None:
                return None
            if self.predicate(item):
                return item
    
class StreamMap(StreamPiece):
    def __init__(self, func, supplyStream):
        self.func = func
        self.supplyStream = supplyStream

    @decor("Map")
    def supplyItem(self):
        item = self.supplyStream.supplyItem()
        return self.func(item) if item != None else None

class StreamLimit(StreamPiece):
    def __init__(self, limit, supplyStream):
        self.limit = limit
        self.supplyStream = supplyStream

    @decor("Limit")
    def supplyItem(self):
        self.limit -= 1
        return self.supplyStream.supplyItem() if self.limit >= 0 else None

class StreamSkip(StreamPiece):
    def __init__(self, skipAmount, supplyStream):
        self.supplyStream = supplyStream
        self.skipAmount = skipAmount

    @decor("Skip")
    def supplyItem(self):
        while self.skipAmount > 0:
            self.supplyStream.supplyItem()
            self.skipAmount -= 1
        return self.supplyStream.supplyItem()

class Reducer:
    def __init__(self, stream, startValue, biFunc):
        self.stream = stream
        self.startValue = startValue
        self.biFunc = biFunc
    
    def reduce(self):
        result = self.startValue
        item = self.stream.supplyItem()
        while item != None:
            result = self.biFunc(result, item)
            item = self.stream.supplyItem()
        return result

    def mutableReduce(self):
        result = self.startValue
        item = self.stream.supplyItem()
        while item != None:
            self.biFunc(result, item)
            item = self.stream.supplyItem()
        return result

class Collector:

    def __init__(self, startValueSupplier, merger):
        self.startValueSupplier = startValueSupplier
        self.merger = merger

class Counter:
    def __init__(self):
        self.count = 0

    def inc(self):
        self.count += 1

    def __str__(self):
        return str(self.count)

    def __repr__(self):
        return str(self.count)

class StringAppender:
    def __init__(self):
        self.contained = ""
        self.isEmpty = True

    def append(self, item):
        self.contained += str(item)
        self.isEmpty = False

    def __str__(self):
        return str(self.contained)

    def __repr__(self):
        return str(self.contained)

class Collectors:
    
    @staticmethod
    def toList():
        def mergerFunc(collected, newItem):
            collected.append(newItem)
        return Collector(lambda: [], mergerFunc)

    @staticmethod
    def toMap(keyMapper, valueMapper):
        def mergerFunc(collected, newItem):
            collected[keyMapper(newItem)] = valueMapper(newItem)
        return Collector(lambda: dict(), mergerFunc)
            
    @staticmethod
    def groupingBy(keyMapper):
        def mergerFunc(collected, newItem):
            key = keyMapper(newItem)
            if key in collected:
                collected[key].append(newItem)
            else:
                collected[key] = [newItem]        
        return Collector(lambda: dict(), mergerFunc)

    @staticmethod
    def groupingByCollect(keyMapper, downCollector):
        def mergerFunc(collected, newItem):
            key = keyMapper(newItem)
            if key in collected:
                downCollector.merger(collected[key], newItem)
            else:
                collected[key] = downCollector.startValueSupplier()
        return Collector(lambda: dict(), mergerFunc)

    @staticmethod
    def counting():
        def mergerFunc(collected, newItem):
            collected.inc()
        return Collector(Counter, mergerFunc)


    @staticmethod
    def mapping(mapper, collector):
        def mergerFunc(collected, newItem):
            collector.merger(collected, mapper(newItem))
        return Collector(collector.startValue, mergerFunc)

    @staticmethod
    def joining(delimiter = ""):
        def mergerFunc(collected, newItem):
            if collected.isEmpty:
                collected.append(newItem)
            else:
                collected.append(delimiter)
                collected.append(newItem)
        return Collector(StringAppender, mergerFunc)

def add(a, b):
    return a + b

def isPrime(n):
    if n < 2:
        return False
    for i in range(2,int(n**0.5)+1):
        if n%i==0:
            return False
    return True

def multiply(a, b):
    return a * b

# Calculating the sum of the first 5 primes:
result = StreamStart(count())\
         .filter(isPrime)\
         .limit(5)\
         .reduce(0, add)
print("The sum of first 5 primes is: " + str(result))

# Calculating 4 factorial:
result = StreamStart(range(1, 5))\
         .reduce(1, multiply)
print("4! equals " + str(result))

# Calculating the sum of the first 10 square numbers:
result = StreamStart(count())\
         .map(lambda x: x **2)\
         .limit(10)\
         .reduce(0, add)
print("The sum of first 10 square numbers (including 0) is: " + str(result))

# Separating primes from non-primes:

result = StreamStart(count())\
         .skip(2)\
         .limit(20)\
         .collect(Collectors.groupingByCollect(isPrime, Collectors.counting()))

print("Count prime / composite numbers from 2 up to 21: " + str(result))
