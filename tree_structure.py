TRANSPARENT = ""

def lte(x, y):
    return x <= y

def lt(x, y):
    return x < y

class StringArt:
    def __init__(self, art, size):
        self.size = size
        self.art = art

    def isInside(self, coord):
        return all(map(lte, (0, 0), coord)) and all(map(lt, coord, self.size))
    
    def getCharAt(self, coord):
        return self.art(coord) if self.isInside(coord) else TRANSPARENT

    def getDisplayChar(self, coord):
        char = self.getCharAt(coord)
        return char if char != TRANSPARENT else " "

    def getSize(self):
        return self.size

    def __add__(self, other):
        return self.add(other)

    def __rshift__(self, vector):
        return self.translate(vector)

    def add(self, other):
        def art(coord):
            myChar = self.getCharAt(coord)
            otherChar = other.getCharAt(coord)
            return myChar if myChar != "" else otherChar
        size = tuple(map(max, self.getSize(), other.getSize()))
        return StringArt(art, size)

    def translate(self, vector):
        def art(coord):
            x, y = coord
            vx, vy = vector
            return self.art((x - vx, y - vy))
        return StringArt(art, self.size)

    def resize(self, newSize):
        return StringArt(self.art, newSize)

    def frame(self, vect):
        def art(coord):
            inside = all(map(lte, (0, 0), coord)) and all(map(lt, coord, vect))
            return self.art(coord) if inside else TRANSPARENT
        return StringArt(art, self.size)

    def __or__(self, vect):
        return self.frame(vect)
    
    def print(self):
        width, height = self.size
        for y in range(height):
            line = "".join(self.getDisplayChar((x, y)) for x in range(width))
            print(line)

def createArt(string):
  lines = string.split("\n")
  def result(coord):
    y, x = coord
    if x >= 0  and x < len(lines):
      if y >= 0 and y < len(lines[x]):
        return spaceToTransparent(lines[x][y])
    return TRANSPARENT  
  return result

def createEmpty():
  return lambda coord: TRANSPARENT

def spaceToTransparent(char):
  return TRANSPARENT if char == " " else char

def getTreeSubPart(height):
    return "+-\n" + ("|\n" * (height - 1))

def getTreePart(heights):
    if len(heights) == 0:
        return StringArt(createEmpty(), (0, 0))
    else:
        string = "".join(getTreeSubPart(height)
                         for height in heights[:-1]) + "\\-"
        totalHeight = sum(heights[:-1]) + 1
    return StringArt(createArt(string), (2, totalHeight))

def emptyStringArt():
    return StringArt(createEmpty(), (0, 0))

def getTreeArt(tree):
    value = str(tree[0])
    subTrees = [getTreeArt(item) for item in tree[1:]]
    heights = [tree.size[1] for tree in subTrees]
    valueArt = StringArt(createArt(value), (len(value), 1))
    totalHeight = sum(heights) + 1
    totalWidth = max([len(value),
                      2 + max([tree.size[0] for tree in subTrees], default = 0)])
    totalSize = (totalWidth, totalHeight)
    subTrees = [subTree.resize(totalSize)
                for subTree in subTrees]
    translation = 1
    for index, tree in enumerate(subTrees):
        subTrees[index] = tree.translate((2, translation))
        translation += heights[index]
    mainBranch = getTreePart(heights).translate((0, 1)).resize(totalSize)
    return sum(subTrees, emptyStringArt()) + valueArt + mainBranch

tree = ["C:",
        ["Program Files",
         ["Java",
          ["jdk9",
           ["bin",
            ["java.exe"],
            ["javac.exe"],
            ["jshell.exe"]
            ]
           ]
          ]
         ],
        ["Python36",
         ["Scripts",
          ["pip.exe"]
          ],
         ["python.exe"]
         ]
        ]
print("Blessed is the one who does not walk in step with the wicked or \
stand in the way \
that sinners take or sit in the company of mockers, \
but whose delight in in the law of the Lord, \
and who meditates on his law day and night. \
That person is like a tree planted by streams of water, \
which yields its fruit in season \
and whose leaf does not wither - \
whatever they do prospers. (Psalms 1, 1 - 3)\n\n")
getTreeArt(tree).print()
