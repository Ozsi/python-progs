import random
import operator
import itertools
# Cretes a random maze
#
# The print formats and the seed generation
# is similar to Kuba's "[Python] Labyrinth generator" programs

# The maze generating algorithm creates a random spanning tree of a
# grid graph using disjoint-set data structure

# Links:
#   Kuba's Labyrinth generator:
#     https://code.sololearn.com/c49Oxtn2gGGQ/#py
#   Spanning tree:
#     https://en.wikipedia.org/wiki/Spanning_tree
#   Disjoint-set data structure:
#     https://en.wikipedia.org/wiki/Disjoint-set_data_structure)

N = (-1, 0)
W = (0, -1)
S = (1, 0)
E = (0, 1)

DIRS = (N, E, S, W)
SIZEX = 20
SIZEY = 15
FIELDS = set(itertools.product(range(SIZEX), range(SIZEY)))
parentMap = {}

#--------------------------------------
# This is the part where the disjoint-set data structure is implemented:
def getParent(node):
  return parentMap.get(node, node)

def setParent(node, parent):
  parentMap[node] = parent

def find(node):
  if getParent(node) == node:
    return node
  else:
    result = find(getParent(node))
    setParent(node, result)
    return result
    
def union(nodeX, nodeY):
  roots = [find(nodeX), find(nodeY)]
  if roots[0] != roots[1]:
    random.shuffle(roots)
    setParent(roots[0], roots[1])

def isAlreadyConnected(nodeX, nodeY):
  return find(nodeX) == find(nodeY)

#---------------------------------------
# Some functions that are required to handle the graph

def addTuples(v1, v2):
  return tuple(map(operator.add, v1, v2))

def createEdge(v1, v2):
  return (v1, v2) if v1 <= v2 else (v2, v1)

def getAllEdges():
  return {edge for edges in map(getIncidentalEdges, FIELDS) for edge in edges}
        
def getIncidentalEdges(field):
  neighbors = getNeighbors(field)
  return map(lambda neighbor: createEdge(neighbor, field), neighbors)

def isInside(field):
  return field in FIELDS

def getNeighbors(field):
  return filter(isInside, map(lambda direction: addTuples(field, direction), DIRS))

def isNodeConnectedTo(node, direction, edges):
  neighbor = addTuples(node, direction)
  edge = createEdge(node, neighbor)
  return edge in edges
  
# Methods for finding the path
def addEdgeToDict(edge, dictionary):
  start, end = edge
  if (start in dictionary):
    dictionary[start].append(end)
  else:
    dictionary[start] = [end]
 
def getConnectionMap(edges):
  result = {}
  for edge in edges:
    addEdgeToDict(edge, result)
    addEdgeToDict(edge[::-1], result)
  return result
      
def backTracePath(start, previousFieldMap):
  current = start
  result = []
  while (current != previousFieldMap[current]):
    result.append(current)
    current = previousFieldMap[current]
  result.append(current)
  return result
      
# Find a path from (0, -1) to (SIZEX - 1, SIZEY) and return the list of coordinates that 
# belong to the path
def findPath(mazeEdges):
  start = (0, -1)
  end = (SIZEX - 1, SIZEY)
  previousFieldMap = {end: end}
  connectionMap = getConnectionMap(mazeEdges)
  queue = [end]
  while len(queue) > 0:
    current = queue.pop(0)
    neighbors = connectionMap[current]
    unvisitedNeighbors = list(filter(lambda neighbor: neighbor not in previousFieldMap, neighbors))
    for neighbor in unvisitedNeighbors:
      previousFieldMap[neighbor] = current
    queue.extend(unvisitedNeighbors)
  if (start in previousFieldMap):
    return backTracePath(start, previousFieldMap)
  else:
    return None


#----------------------------------------
# This part generates the maze itself

def generateMazeEdges(edgeList):
  entranceEdge = ((0, -1), (0, 0))
  exitEdge = ((SIZEX - 1, SIZEY - 1), (SIZEX - 1, SIZEY))
  mazeEdges = {entranceEdge, exitEdge}
  for edge in edgeList:
    node1, node2 = edge
    if not isAlreadyConnected(node1, node2):
      union(node1, node2)
      mazeEdges |= {edge}
  return mazeEdges


#--------------------------------------
# Methods that are responsible for printing

def getWallSymbol(x, y, edges):
  leftNeighbor = (x, y)
  rightNeighbor = (x, y + 1)
  isVerticalLine = not isNodeConnectedTo(leftNeighbor, E, edges)
  isHorizontalLine = not isNodeConnectedTo(leftNeighbor, S, edges) and not isNodeConnectedTo(rightNeighbor, S, edges)
  return "|" if isVerticalLine else "_" if isHorizontalLine else " "

def getFieldSymbol(x, y, edges):
  thisField = (x, y)
  vertical = not isNodeConnectedTo(thisField, S, edges)
  return "_" if vertical else " "

def getSymbol(row, column, edges):
  x = row
  y = column // 2
  wall = column % 2 == 0
  return getWallSymbol(x, y - 1, edges) if wall else getFieldSymbol(x, y, edges)

def getSymbolStyle2(row, column, edges, path):
  isField = row % 2 == 1 and column % 2 == 1
  isVertWall = row % 2 == 0 and column % 2 == 1
  isHorWall = row % 2 == 1 and column % 2 == 0
  isInside = row % 2 == 0 and column % 2 == 0
  EMPTY = " "
  PATH = "."
  OCCUPIED = "#"
  if isField:
    field = (row // 2, column // 2)
    return PATH if field in path else EMPTY
  if isVertWall:
    leftNeighbor = (row // 2 - 1, column // 2)
    rightNeighbor = (row // 2, column // 2)
    if createEdge(leftNeighbor, rightNeighbor) in edges:
      return PATH if leftNeighbor in path and rightNeighbor in path else EMPTY
    return OCCUPIED
  if isHorWall:
    upNeighbor = (row // 2, column // 2 - 1)
    downNeighbor = (row // 2, column // 2)
    if createEdge(upNeighbor, downNeighbor) in edges:
      return PATH if upNeighbor in path and downNeighbor in path else EMPTY
    return OCCUPIED
  return OCCUPIED
  

def getMazeLine(row, mazeEdges, sizeY):
  line = [getSymbol(row, column, mazeEdges) for column in range(2 * sizeY + 1)]
  return "".join(line)

def getMazeLineStyle2(row, mazeEdges, path, sizeY):
  line = [getSymbolStyle2(row, column, mazeEdges, path) for column in range(2 * sizeY + 1)]
  return "".join(line)

def printMaze(sizeX, sizeY, mazeEdges):
  mazeLines = ["_" * (2 * sizeY + 1)]
  mazeLines += [getMazeLine(row, mazeEdges, sizeY) for row in range(sizeX)]
  mazeString = "\n".join(mazeLines)
  print(mazeString)

def printMazeStyle2(sizeX, sizeY, mazeEdges, path):
  mazeLines = []
  mazeLines += [getMazeLineStyle2(row, mazeEdges, path, sizeY) for row in range(2 * sizeX + 1)]
  mazeString = "\n".join(mazeLines)
  print(mazeString)

#------------------------------------
# The main method

def main():
  EDGESET = getAllEdges()
  edgeList = list(EDGESET)
  random.shuffle(edgeList)
  mazeEdges = generateMazeEdges(edgeList)
  path = findPath(mazeEdges)
  print("Maze without solution:")
  print("(solution below)")
  printMaze(SIZEX, SIZEY, mazeEdges)
  print("\nMaze with solution:")
  printMazeStyle2(SIZEX, SIZEY, mazeEdges, path)
  

seed = random.randint(0, 1000)
random.seed(seed)
print("Maze size: " + str(SIZEX) + "x" + str(SIZEY)) 
print("Seed is #" + str(seed))
print("\"I will instruct you and teach you in the way you should go; I will counsel you with my loving eye on you.\" (Psalms 32, 8)\n")
main()

