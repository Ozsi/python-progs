from itertools import product
from itertools import chain

def readNames(n):
    result = []
    print("Enter list of %s names separated by newlines:" % n)
    for i in range(n):
        result.append(input())
    return tuple(result)

def generatePoints(p):
    first = map(lambda x: (1, x[0], x[1]), product(range(p), range(p)))
    second = map(lambda x: (0, 1, x), range(p))
    third = [(0, 0, 1)]
    return tuple(chain(first, second, third))

def isOrthogonal(v1, v2, prime):
    return sum([i*j for (i, j) in zip(v1, v2)]) % prime == 0
    
def getPrime():
    print("Enter a prime number p.\nThe cards will have p + 1 symbols on them.")
    return int(input())

def generateCard(line, prime, nameList, pointList):
    result = []
    for i in range(len(pointList)):
      if isOrthogonal(pointList[i], line, prime):
        result.append(nameList[i])
    return tuple(result)

def generateCards(prime, nameList):
    lineList = generatePoints(prime)
    return tuple(generateCard(line, prime, nameList, lineList) for line in lineList)

def printCards(cards):
    for i in range(len(cards)):
        print("#%s: " % (i + 1), end = "")
        print(", ".join(cards[i]))

prime = getPrime()
size = prime ** 2 + prime + 1
nameList = readNames(size)
cards = generateCards(prime, nameList)

printCards(cards)

