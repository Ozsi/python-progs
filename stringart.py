from itertools import product

TRANSPARENT = ""

def spaceToTransparent(char):
  return TRANSPARENT if char == " " else char

def createEmpty():
  """
    Creates an empty StringArt
  """
  return lambda coord: TRANSPARENT

def create(string):
  """
    Creates a StringArt from a string
  """
  lines = string.split("\n")
  def result(coord):
    x, y = coord
    if x >= 0  and x < len(lines):
      if y >= 0 and y < len(lines[x]):
        return spaceToTransparent(lines[x][y])
    return TRANSPARENT  
  return result

def cacheArt(stringArt, dimension):
  """
    Caches an existing StringArt
  """
  sizeX, sizeY = dimension
  string = "\n".join("".join(resolveChar(stringArt((i, j)))
                              for j in range(sizeY))
                     for i in range(sizeX))
  return create(string)  
    
def translate(stringArt, vect):
  """
    Translates a StringArt to a different position
  """
  return lambda coord: stringArt((coord[0] - vect[0], coord[1] - vect[1]))

def transpose(stringArt):
  """
     Transposes a StringArt by switching x and y coordinates
  """
  return lambda coord: stringArt(coord[::-1])

def union(stringArts):
  """
    Takes a list of StringArts and returns a stringArt that
    returns the first non-transparent value
  """
  def result(coord):
    x, y = coord
    for art in stringArts:
      if art(coord) != TRANSPARENT:
        return art(coord)
    return TRANSPARENT
  return result
  
def compose(stringArt, func):
  """
    Applies a string->string function to a stringArt.
  """
  return lambda coord: func(stringArt(coord))

def frame(art, dimension):
  """
    Creates a frame around a StringArt of size sizeX x sizeY
    Example:
     art = abcd111
           efgh222
           ijkl333
           mnop444
     then frameArt(art, 4, 4) =
           +--+
           |ab|
           |ef|
           +--+
  """
  sizeX, sizeY = dimension
  xRange = range(sizeX)
  yRange = range(sizeY)
  xInside = range(1, sizeX - 1)
  yInside = range(1, sizeY - 1)
  translatedArt = translate(art, (1, 1))
  def result(coord):
    x, y = coord
    if (x not in xRange) or (y not in yRange):
      return TRANSPARENT
    if (x not in xInside) and (y not in yInside):
      return "+"
    if x not in xInside:
      return "-"
    if y not in yInside:
      return "|"
    return translatedArt(coord)
  return result

def createSizedArt(size, art):
  """
    Creates a sized art
  """
  return {"size": size, "art": art}

def resize(sizedArt, size):
  """
    Resizes a sized art
  """
  return createSizedArt(size, sizedArt["art"])

def createEmptySizedArt():
  """
    Creates an empty sized art
  """
  return createSizedArt((0, 0), createEmpty())

def addFrameToSize(size):
  return (size[0] + 2, size[1] + 2)

def frameSizedArt(sizedArt):
  return frame(sizedArt["art"], addFrameToSize(sizedArt["size"]))

def getWidth(sizedArt):
  return sizedArt["size"][1]

def getHeight(sizedArt):
  return sizedArt["size"][0]

def getMaxStat(fieldList, stat):
  return max([stat(field) for field in fieldList])

def getRow(fields, row, columns):
  empty = createEmptySizedArt()
  return [fields.get((row, y), empty) for y in range(columns)]

def getColumn(fields, column, rows):
  empty = createEmptySizedArt()
  return [fields.get((x, column), empty) for x in range(rows)]

def getRowHeight(fields, row, columns):
  return getMaxStat(getRow(fields, row, columns), getHeight)

def getColumnWidth(fields, column, rows):
  return getMaxStat(getColumn(fields, column, rows), getWidth)

def getCellHeightsAndWidths(dimension, fields):
  rows, columns = dimension  
  heights = [getRowHeight(fields, x, columns) for x in range(rows)]
  widths = [getColumnWidth(fields, y, rows) for y in range(columns)]
  return (heights, widths)

def getSeparators(rows, heights):
  return [sum(heights[:i]) + i for i in range(rows + 1)]

def getSizeGetter(widths, heights):
  def result(pos):
    i, j = pos
    return (heights[i], widths[j])
  return result

def getFramedArtForPos(pos, fields, sizeGetter):
  empty = createEmptySizedArt()
  return frameSizedArt(resize(fields.get(pos, empty), sizeGetter(pos)))

def getFieldPositions(dimension):
  rows, columns = dimension
  return product(range(rows), range(columns))

def getFramedFields(fields, sizeGetter, dimension):
  fieldPositions = getFieldPositions(dimension)
  return {pos: getFramedArtForPos(pos, fields, sizeGetter)
          for pos in fieldPositions}

def getMatchingIndex(x, xSeparators):
  return len(list(filter(lambda n: n <= x, xSeparators[:-1]))) - 1

def getFieldSelector(xSeparators, ySeparators):
  def result(coord):
    x, y = coord
    i = getMatchingIndex(x, xSeparators)
    j = getMatchingIndex(y, ySeparators)
    return (i, j)
  return result

def getOffset(x, xSeparators):
  return x - max(filter(lambda n: n <= x, xSeparators), default = -1)

def getOffsetCalc(xSeparators, ySeparators):
  def result(coord):
    x, y = coord
    xOffset = getOffset(x, xSeparators)
    yOffset = getOffset(y, ySeparators)
    return (xOffset, yOffset)
  return result

def getTableSize(xSeparators, ySeparators):
  return (xSeparators[-1] + 1, ySeparators[-1] + 1)

def createTable(dimension, fields):
  """
    Creates a table of sizedArts from a dictionary
  """
  rows, columns = dimension
  heights, widths = getCellHeightsAndWidths(dimension, fields)
  
  xSeparators = getSeparators(rows, heights)
  ySeparators = getSeparators(columns, widths)
  sizeGetter = getSizeGetter(widths, heights)
  
  resultSize = getTableSize(xSeparators, ySeparators)
  framedFields = getFramedFields(fields, sizeGetter, dimension)
  fieldSelector = getFieldSelector(xSeparators, ySeparators)
  offsetCalculator = getOffsetCalc(xSeparators, ySeparators)
  empty = createEmptySizedArt()
  def resultArt(coord):
    selectedPos = fieldSelector(coord)
    offset = offsetCalculator(coord)
    return framedFields.get(selectedPos, empty)(offset)
  return createSizedArt(resultSize, resultArt)


#-------------------------------------
# Methods that are responsible for displaying StringArts

def resolveChar(char):
  """
    Resolves a character from a StringArt into a printable format
  """
  return  " " if char == TRANSPARENT else char

def printArt(stringArt, dimension):
  """
    Prints a StringArt in a rectangle of sizeX x sizeY
  """
  sizeX, sizeY = dimension
  for i in range(sizeX):
    line = "".join(resolveChar(stringArt((i, j))) for j in range(sizeY))
    print(line)

def printSizedArt(sizedArt):
  printArt(sizedArt["art"], sizedArt["size"])

#-----------------------------------

def getSizedArtFromProvider(stringProvider, pos):
  artString = stringProvider(pos)
  height = 0 if artString == "" else 1
  size = (height, len(artString))
  return createSizedArt(size, create(artString))

def createTableDict(dimension, stringProvider):
  positions = getFieldPositions(dimension)
  return {pos: getSizedArtFromProvider(stringProvider, pos) for pos in positions}


def getMultiplicationTableStringProvider():
  def result(pos):
    i, j = pos
    if i == 0 and j == 0:
      return "*"
    if i == 1 or j == 1:
      return ""
    if i == 0 or j == 0:
      return str(max(i - 2, j - 2))
    return str((i - 2) * (j - 2))
  return result

provider = getMultiplicationTableStringProvider()
tableDict = createTableDict((13, 13), provider)
tableArt = createTable((13, 13), tableDict)
printSizedArt(tableArt)


## TMP
a = {
  "size": (1, 1),
  "art": create("aa\naa")
}
b = {
  "size": (1, 7),
  "art": create("bb\nbb")
}
c = {
  "size": (4, 4),
  "art": create("cc\ncc")
}
d = {
  "size": (4, 7),
  "art": create("ddddddddddddd\ndd")
}

fields = {
  (0, 0): a,
  (0, 1): b,
  (1, 0): c,
  (1, 1): d
}

missingFields = {
  (0, 0): a,
  (0, 1): b,
  (1, 0): c
}



printSizedArt(createTable((2, 2), missingFields))
printSizedArt(createTable((2, 2), fields))
