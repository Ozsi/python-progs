# Example solution in Python for "Color the building" challenge.

colorDictionary = {
    'r': "Red",
    'b': "Blue"
}

def appendBlueFloor(coloring):
    return coloring + "b"

def appendBlueAndRedFloor(coloring):
    return coloring + "br"

def getColorings(floors):
    if floors <= 0:
        yield ""
    if floors == 1:
        yield from ("b", "r")
    if floors > 1:
        yield from map(appendBlueFloor, getColorings(floors - 1))
        yield from map(appendBlueAndRedFloor, getColorings(floors - 2))

def toReadable(coloring):
    return ", ".join(colorDictionary[color] for color in coloring)

def main():
    try:
        floors = int(input())
    except:
        floors = 1

    counter = 0
    for coloring in getColorings(floors):
        print(toReadable(coloring))
        print()
        counter +=1
    print("There are %s good colorings." % counter)

main()
