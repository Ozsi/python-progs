# Desribes a short story of a man using an ATM
# Ascii arts from
# http://www.chris.com/ascii/joan/www.geocities.com/SoHo/7373/small.html

TRANSPARENT = ""
def empty():
  """
    Creates an empty StringArt
  """
  return lambda x, y: TRANSPARENT

def spaceToTransparent(char):
  return TRANSPARENT if char == " " else char

def create(string):
  """
    Creates a StringArt from a string
  """
  lines = string.split("\n")
  def result(x, y):
    if x >= 0  and x < len(lines):
      if y >= 0 and y < len(lines[x]):
        return spaceToTransparent(lines[x][y])
    return TRANSPARENT  
  return result
    
def translate(stringArt, i, j):
  """
    Translates a StringArt to a different position
  """
  return lambda x, y: stringArt(x - i, y - j)

def transpose(stringArt):
  """
     Transposes a StringArt by switching x and y coordinates
  """
  return lambda x, y: stringArt(y, x)

def union(stringArts):
  """
    Takes a list of StringArts and returns a stringArt that
    returns the first non-transparent value
  """
  def result(x, y):
    for art in stringArts:
      if art(x, y) != TRANSPARENT:
        return art(x, y)
    return TRANSPARENT
  return result
  
def compose(stringArt, func):
  """
    Applies a string->string function to a stringArt.
  """
  return lambda x, y: func(stringArt(x, y))

def frame(art, sizeX, sizeY):
  """
    Creates a frame around a StringArt of size sizeX x sizeY
    Example:
     art = abcd111
           efgh222
           ijkl333
           mnop444
     then frameArt(art, 4, 4) =
           +--+
           |ab|
           |ef|
           +--+
  """
  xRange = range(sizeX)
  yRange = range(sizeY)
  xInside = range(1, sizeX - 1)
  yInside = range(1, sizeY - 1)
  def result(x, y):
    if (x not in xRange) or (y not in yRange):
      return TRANSPARENT
    if (x not in xInside) and (y not in yInside):
      return "+"
    if x not in xInside:
      return "-"
    if y not in yInside:
      return "|"
    return art(x - 1, y - 1)
  return result



#-------------------------------------
# Methods that are responsible for displaying StringArts

def resolveChar(char):
  """
    Resolves a character from a StringArt into a printable format
  """
  return  " " if char == TRANSPARENT else char

def printArt(stringArt, sizeX, sizeY):
  """
    Prints a StringArt in a rectangle of sizeX x sizeY
  """
  for i in range(sizeX):
    line = "".join([resolveChar(stringArt(i, j)) for j in range(sizeY)])
    print(line)

#------------------------------------
# The scene itself

man = create(" O\n<|\\\n |\n/ \\")
manBack = create(" O\n/|>\n |\n/ \\")
car = create("       .--.      \n  .----'   '--.  \n  '-()-----()-'")
atm = translate(create("[ATM]\n  |\n  |"), 1, 25)
theEnd = translate(create("THE END"), 1, 12)

MAN_GETS_OUT = 20
MAN_ARRIVES_AT_ATM = 35
MAN_LEAVES_ATM = 40
MAN_GETS_IN = 55

HEIGHT = 6
WIDTH = 33

def getMan(frame):
    x = 0
    y = 2 * (frame - MAN_GETS_OUT) // 3 + 13
    y = 22 if frame >= MAN_ARRIVES_AT_ATM else y
    y = 22 - 2 * (frame - MAN_LEAVES_ATM) // 3 if frame >= MAN_LEAVES_ATM else y
    visible = frame >= MAN_GETS_OUT and frame < MAN_GETS_IN
    baseArt = man if frame < MAN_LEAVES_ATM else manBack
    return empty() if not visible else translate(baseArt, x, y)

def getCar(frame):
    x = 1
    y = frame - 15
    y = 5 if frame > MAN_GETS_OUT else y
    y = 5 + frame - MAN_GETS_IN if frame > MAN_GETS_IN else y
    return translate(car, x, y)

def getAtm(frame):
    return atm

def main():
  for frameNo in range(0, 81, 5):
    scene = union([getMan(frameNo), getAtm(frameNo), getCar(frameNo)])
    printArt(frame(scene, HEIGHT, WIDTH), HEIGHT, WIDTH)
  printArt(frame(theEnd, HEIGHT, WIDTH), HEIGHT, WIDTH)

main()
