
INVALID = "Invalid page number, pleae try again."

pages = {
    1: """You are standing on a corridor. There is a door in front of you and to the left.
Which one do you choose?
The door in front - Go to page 4
The door to the left - Go to page 8""",
    3: """\"\"""",
    4: """You walk through the corridor and open the door. It opens with a loud crack.
You enter a room that you can tell none has been in for years. There is a desk
here. You see a Bible lying there. What do you do?
Open the Bible and read a verse - go to page 3
Leave the room where you came from - go to page 7"""}

def getInt():
    try:
        return int(input())
    except ValueError:
        return 1

def printFooter(page):
    if page == 1:
        print("To go to a page run the program again and use the page number as an input.")

def printPage(page):
    if page in pages:
        print("Page #%s:" % page)
        print(pages.get(page))
        printFooter(page)        
    else:
        print(INVALID)

page = getInt()
printPage(page)


