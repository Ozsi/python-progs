TRANSPARENT = ""

def lte(x, y):
    return x <= y

def lt(x, y):
    return x < y

class StringArt:
    def __init__(self, art, size):
        self.size = size
        self.art = art

    def isInside(self, coord):
        return all(map(lte, (0, 0), coord)) and all(map(lt, coord, self.size))
    
    def getCharAt(self, coord):
        return self.art(coord) if self.isInside(coord) else TRANSPARENT

    def getDisplayChar(self, coord):
        char = self.getCharAt(coord)
        return char if char != TRANSPARENT else " "

    def getSize(self):
        return self.size

    def __add__(self, other):
        return self.add(other)

    def __rshift__(self, vector):
        return self.translate(vector)

    def add(self, other):
        def art(coord):
            myChar = self.getCharAt(coord)
            otherChar = other.getCharAt(coord)
            return myChar if myChar != "" else otherChar
        size = tuple(map(max, self.getSize(), other.getSize()))
        return StringArt(art, size)

    def translate(self, vector):
        def art(coord):
            x, y = coord
            vx, vy = vector
            return self.art((x - vx, y - vy))
        return StringArt(art, self.size)

    def __xor__(self, newSize):
        return self.resize(newSize)

    def resize(self, newSize):
        return StringArt(self.art, newSize)
    
    def transpose(self):
        def art(coord):
            x, y = coord
            return self.art((y, x))
        width, height = self.size
        return StringArt(art, (height, width))

    def frame(self, vect):
        def art(coord):
            inside = all(map(lte, (0, 0), coord)) and all(map(lt, coord, vect))
            return self.art(coord) if inside else TRANSPARENT
        return StringArt(art, self.size)

    def __or__(self, vect):
        return self.frame(vect)
    
    def __invert__(self):
        return self.transpose()
    
    def print(self):
        width, height = self.size
        for y in range(height):
            line = "".join(self.getDisplayChar((x, y)) for x in range(width))
            print(line)
    
empty = StringArt(lambda x: TRANSPARENT, (5, 5))
full = StringArt(lambda x: "#", (6, 6))

def binaryArt(predicate, filled, empty):
    return lambda coord: filled if predicate(coord) else empty

def inX(coord):
    x, y = coord
    return abs(x) == abs(y)

xArt = StringArt(binaryArt(inX, "X", TRANSPARENT), (5, 5)) >> (2, 2)
xArt = xArt
(xArt + full).print()
