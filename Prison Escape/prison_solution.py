# This code is an example solution for the Prison Escape Challenge.


WALL = "#"
EXIT = "E"
START = "P"
PETE = "P"
FLOOR = " "

GEM_CHARACTERS = set(map(str, range(10)))
DOOR_CHARACTERS = set(c for c in "abcdefghij")
WALL_CHARACTERS = {WALL}
EMPTY_CHARACTERS = {START, FLOOR, EXIT}

# Methods responsible for displaying the solution

TRANSPARENT = ""


def spaceToTransparent(char):
  return TRANSPARENT if char == " " else char

def createStringArt(string):
        lines = string.split("\n")
        def result(coord):
            x, y = coord
            if x >= 0 and x < len(lines):
                if y >= 0 and y < len(lines[x]):
                    return spaceToTransparent(lines[x][y])
            return TRANSPARENT  
        return result

def cacheArt(stringArt, dimension):
  sizeX, sizeY = dimension
  string = "\n".join("".join(resolveChar(stringArt((i, j)))
                              for j in range(sizeY))
                     for i in range(sizeX))
  return createStringArt(string)

def resolveChar(char):
  return " " if char == TRANSPARENT else char

def printArt(stringArt, dimension):
  sizeX, sizeY = dimension
  for i in range(sizeX):
    line = "".join(resolveChar(stringArt((i, j))) for j in range(sizeY))
    print(line)

def union(*stringArts):
  def result(coord):
    for art in stringArts:
      if art(coord) != TRANSPARENT:
        return art(coord)
    return TRANSPARENT
  return result

def pathArt(path, visited):
    if len(path) == 0:
        return union()
    coordDict = {coord: " " for coord in visited}
    for i, coord in enumerate(path):
        nextCoord = path[i+1] if i+1 < len(path) else coord
        if nextCoord[0] != coord[0]:
            coordDict[coord] = "|"
        if nextCoord[1] != coord[1]:
            coordDict[coord] = "-"
    coordDict[path[-1]] = PETE
    def result(coord):
        return coordDict.get(coord, TRANSPARENT)
    return result


def addVectors(*vectors):
            return tuple(map(sum, zip(*vectors)))
            
# A class that desribes a prison

class Prison:
    def __init__(self, string):
        self.__string = string
        self.__lines = string.split("\n")
        self.__positions = self.__getPositions(string)
        self.__size = self.__calculateSize(self.__positions)
        self.__stringArt = cacheArt(createStringArt(string), self.__size)

    def print(self):
        print(self.__string)

    def getStringArt(self):
        return self.__stringArt
        
    def getSize(self):
        return self.__size
        
    def getCharAt(self, coord):
        if coord not in self.__positions:
            return WALL
        row, column = coord
        return self.__lines[row][column]

    def isGem(self, coord):
        return self.getCharAt(coord) in GEM_CHARACTERS

    def isDoor(self, coord):
        return self.getCharAt(coord) in DOOR_CHARACTERS

    def isEmpty(self, coord):
        return self.getCharAt(coord) in EMPTY_CHARACTERS

    def isWall(self, coord):
        return self.getCharAt(coord) in WALL_CHARACTERS

    def isExit(self, coord):
        return self.getCharAt(coord) == EXIT

    def __getPositions(self, string):
        x = 0
        y = 0
        result = set()
        for character in string:
            if character == "\n":
                x += 1
                y = 0
            else:
                result.add((x, y))
                y += 1
        return result

    def __calculateSize(self, positions):
        height = max(map(lambda x: x[0], positions)) + 1
        width = max(map(lambda x: x[1], positions)) + 1
        return (height, width)

    def findPositionsOf(self, char):
        for coord in self.__positions:
            if self.getCharAt(coord) == char:
                yield coord
                
    def findFirstPosition(self, char):
        for coord in self.findPositionsOf(char):
            return coord
        return None
    
    def findExit(self):
        return self.findFirstPosition(EXIT)

    def findStart(self):
        return self.findFirstPosition(START)

    def getAccessibleNeighbors(self, coord, hasGem = False):
        offsets = ((0, 1), (1, 0), (0, -1), (-1, 0))
        
        neighbors = set(addVectors(coord, offset) for offset in offsets)
        def isAccessible(coord):
            return self.isEmpty(coord) or self.isGem(coord) or\
                (self.isDoor(coord) and hasGem)
        accessibleNeighbors = set(filter(isAccessible, neighbors))        
        return accessibleNeighbors

def hasGems(visitedDoorsAndGems):
    weight = lambda c: int(c in GEM_CHARACTERS) - int(c in DOOR_CHARACTERS)
    return sum(map(weight, visitedDoorsAndGems)) > 0

def calcPrevStateDict(prison):
    start = prison.findStart()
    goal = prison.findExit()
    
    startState = (start, "")
    prevStateDict = dict()
    queue = [startState]
    def recordNewState(state, previousState):
        if state not in prevStateDict:
            queue.append(state)
            prevStateDict[state] = previousState
    
    while len(queue) > 0:
        currentState = queue.pop(0)
        position, visitedDoorsAndGems = currentState
        if position == goal:
            break
        neighbors = prison.getAccessibleNeighbors(position,
                                                  hasGems(visitedDoorsAndGems))
        for neighbor in neighbors:
            if prison.isEmpty(neighbor):
                recordNewState((neighbor, visitedDoorsAndGems),
                            currentState)
                
            if prison.isGem(neighbor) or prison.isDoor(neighbor):
                c = prison.getCharAt(neighbor)
                if c in visitedDoorsAndGems:
                    recordNewState((neighbor, visitedDoorsAndGems),
                                currentState)
                else:
                    recordNewState((neighbor, visitedDoorsAndGems + c),
                                currentState)
    return prevStateDict

def getPathBackwards(prison):
    goal = prison.findExit()
    start = prison.findStart()
    startState = (start, "")
    prevStateDict = calcPrevStateDict(prison)
    for key in prevStateDict:
        if key[0] == goal:
            endState = key
            break
    else:
        return []
    currentState = endState
    while currentState != startState:
        yield currentState[0]
        currentState = prevStateDict.get(currentState)
    yield start

def getSlicedPath(prison):
    path = tuple(getPathBackwards(prison))[::-1]
    result = [[]]
    visitedCoords = set()
    for coord in path:
        alreadyVisited = coord in visitedCoords
        result[-1].append(coord)
        if (prison.isGem(coord) or prison.isDoor(coord))\
           and not alreadyVisited:
            result.append([coord])
        visitedCoords.add(coord)
    return result

def getExplanation(prison, goalCoord):
    c = prison.getCharAt(goalCoord)
    if prison.isDoor(goalCoord):
        return "Open door " + c
    if prison.isGem(goalCoord):
        return "Get gem " + c
    if prison.isExit(goalCoord):
        return "Go to the exit"

def printPrisonWithPaths(prison):
    paths = getSlicedPath(prison)
    visited = set()
    size = prison.getSize()
    for path in paths:
        if len(path) > 0:
            printArt(union(pathArt(path, visited), prison.getStringArt()), size)
            print(getExplanation(prison, path[-1]))
            visited = visited | set(path)
            print()
    if len(paths[0]) == 0:
        print("This prison is impossible to escape from :(\n")

def analysePrison(prison):
    print("The prison:")
    prison.print()
    print("\nEscape plan:")
    printPrisonWithPaths(prison)
    print("-" * 40)

prison = Prison("""\
#########
#0  c  1#
### ### #
#P      #
# #####b#
# #   #a#
#   # #E#
#########\
""")
print("Prison escape:")
print("Pete (P) is trapped in a prison. He wants to find \n\
the exit (E) to escape.")
print("The prison has walls (#), doors (a-j) and gems (0-9). \n\
Each door requires one gem to open.\n")
analysePrison(prison)
