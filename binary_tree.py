TRANSPARENT = ""

def lte(x, y):
    return x <= y

def lt(x, y):
    return x < y

class StringArt:
    def __init__(self, art, size):
        self.size = size
        self.art = art

    def isInside(self, coord):
        return all(map(lte, (0, 0), coord)) and all(map(lt, coord, self.size))
    
    def getCharAt(self, coord):
        return self.art(coord) if self.isInside(coord) else TRANSPARENT

    def getDisplayChar(self, coord):
        char = self.getCharAt(coord)
        return char if char != TRANSPARENT else " "

    def getSize(self):
        return self.size

    def __add__(self, other):
        return self.add(other)

    def __rshift__(self, vector):
        return self.translate(vector)

    def add(self, other):
        def art(coord):
            myChar = self.getCharAt(coord)
            otherChar = other.getCharAt(coord)
            return myChar if myChar != "" else otherChar
        size = tuple(map(max, self.getSize(), other.getSize()))
        return StringArt(art, size)

    def translate(self, vector):
        def art(coord):
            x, y = coord
            vx, vy = vector
            return self.art((x - vx, y - vy))
        return StringArt(art, self.size)

    def resize(self, newSize):
        return StringArt(self.art, newSize)

    def frame(self, vect):
        def art(coord):
            inside = all(map(lte, (0, 0), coord)) and all(map(lt, coord, vect))
            return self.art(coord) if inside else TRANSPARENT
        return StringArt(art, self.size)

    def __or__(self, vect):
        return self.frame(vect)
    
    def print(self):
        width, height = self.size
        for y in range(height):
            line = "".join(self.getDisplayChar((x, y)) for x in range(width))
            print(line)

def createArt(string):
  """
    Creates a StringArt from a string
  """
  lines = string.split("\n")
  def result(coord):
    y, x = coord
    if x >= 0  and x < len(lines):
      if y >= 0 and y < len(lines[x]):
        return spaceToTransparent(lines[x][y])
    return TRANSPARENT  
  return result

def spaceToTransparent(char):
  return TRANSPARENT if char == " " else char

def getTree(leftInc, rightExc):
    if (leftInc >= rightExc - 1):
        return {"value": "=" + str(leftInc) + "!", "left": None, "right": None}
    else:
        value = (leftInc + rightExc) // 2
        return {
            "value": "<" + str(value) + "?",
            "left": getTree(leftInc, value),
            "right": getTree(value, rightExc)
            }

def getTreePart(height):
    return StringArt(createArt("|\\\n" + ("|\n" * height) + " \\"), (2, height + 2))

def mysum(iterable):
    result = None
    for item in iterable:
        if result == None:
            result = item
        else:
            result = result + item
    return result

def getTreeArt(tree):
    value = tree["value"]
    valueArt = StringArt(createArt(str(value)), (len(value), 1))
    if tree["left"] == None and tree["right"] == None:
        return valueArt
    else:
        first = getTreeArt(tree["left"]) >> (2, 2)
        firstHeight = first.getSize()[1]
        second = getTreeArt(tree["right"]) >> (2, firstHeight + 3)
        secondHeight = second.getSize()[1]
        mainBranch = getTreePart(firstHeight) >> (0, 1)
        totalHeight = 3 + firstHeight + secondHeight
        totalWidth = max(len(value), 2 + second.getSize()[0],
                         2 + first.getSize()[0])
        totalSize = (totalWidth, totalHeight)
        return mysum(map(lambda x: x.resize(totalSize),
                         [first, second, mainBranch, valueArt]))

print("For I am the Lord your God who takes hold of your right hand and says to you, Do not fear; I will help you. (Isaiah 41, 13)\n")

print("Decision tree to guess a number between 0 and 7:")
getTreeArt(getTree(0, 8)).print()
