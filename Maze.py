import random
import operator
import itertools
# Cretes a random maze
#
# The print format and the seed generation
# is similar to Kuba's "[Python] Labyrinth generator"

# The maze generating algorithm creates a random spanning tree of a
# grid graph using disjoint-set data structure

# Links:
#   Kuba's Labyrinth generator:
#     https://code.sololearn.com/c49Oxtn2gGGQ/#py
#   Spanning tree:
#     https://en.wikipedia.org/wiki/Spanning_tree
#   Disjoint-set data structure:
#     https://en.wikipedia.org/wiki/Disjoint-set_data_structure)

N = (-1, 0)
W = (0, -1)
S = (1, 0)
E = (0, 1)

DIRS = (N, E, S, W)
SIZEX = 20
SIZEY = 20
FIELDS = set(itertools.product(range(SIZEX), range(SIZEY)))
parentMap = {}

#--------------------------------------
# This is the part where the disjoin-set data structure is implemented:
def getParent(node):
  return parentMap.get(node, node)

def setParent(node, parent):
  parentMap[node] = parent

def find(node):
  if getParent(node) == node:
    return node
  else:
    result = find(getParent(node))
    setParent(node, result)
    return result
    
def union(nodeX, nodeY):
  roots = [find(nodeX), find(nodeY)]
  if roots[0] != roots[1]:
    random.shuffle(roots)
    setParent(roots[0], roots[1])

def isAlreadyConnected(nodeX, nodeY):
  return find(nodeX) == find(nodeY)

#---------------------------------------
# Some functions that are required to handle the graph

def addTuples(v1, v2):
  return tuple(map(operator.add, v1, v2))

def createEdge(v1, v2):
  return (v1, v2) if v1 <= v2 else (v2, v1)

def getAllEdges():
  return {edge for edges in map(getIncidentalEdges, FIELDS) for edge in edges}
        
def getIncidentalEdges(field):
  neighbors = getNeighbors(field)
  return map(lambda neighbor: createEdge(neighbor, field), neighbors)

def isInside(field):
  return field in FIELDS

def getNeighbors(field):
  return filter(isInside, map(lambda direction: addTuples(field, direction), DIRS))

def isNodeConnectedTo(node, direction, edges):
  neighbor = addTuples(node, direction)
  edge = createEdge(node, neighbor)
  return edge in edges

#----------------------------------------
# This part generates the maze itself

def generateMazeEdges(edgeList):
  mazeEdges = {((0, -1), (0, 0)), ((SIZEX - 1, SIZEY - 1), (SIZEX - 1, SIZEY))}
  for edge in edgeList:
    node1, node2 = edge
    if not isAlreadyConnected(node1, node2):
      union(node1, node2)
      mazeEdges |= {edge}
  return mazeEdges

#--------------------------------------
# Methods that are responsible for printing

def getWallSymbol(x, y, edges):
  leftNeighbor = (x, y)
  rightNeighbor = (x, y + 1)
  isVerticalLine = not isNodeConnectedTo(leftNeighbor, E, edges)
  isHorizontalLine = not isNodeConnectedTo(leftNeighbor, S, edges) and not isNodeConnectedTo(rightNeighbor, S, edges)
  return "|" if isVerticalLine else "_" if isHorizontalLine else " "

def getFieldSymbol(x, y, edges):
  thisField = (x, y)
  vertical = not isNodeConnectedTo(thisField, S, edges)
  return "_" if vertical else " "

def getSymbol(row, column, edges):
  x = row
  y = column // 2
  wall = column % 2 == 0
  return getWallSymbol(x, y - 1, edges) if wall else getFieldSymbol(x, y, edges)

def getMazeLine(row, mazeEdges, sizeY):
  line = [getSymbol(row, column, mazeEdges) for column in range(2 * sizeY + 1)]
  return "".join(line)

def printMaze(sizeX, sizeY, mazeEdges):
  mazeLines = ["_" * (2 * sizeY + 1)]
  mazeLines += [getMazeLine(row, mazeEdges, sizeY) for row in range(sizeX)]
  mazeString = "\n".join(mazeLines)
  print(mazeString)

#------------------------------------
# The main method

def main():
  EDGESET = getAllEdges()
  edgeList = list(EDGESET)
  random.shuffle(edgeList)
  mazeEdges = generateMazeEdges(edgeList)
  printMaze(SIZEX, SIZEY, mazeEdges)

seed = random.randint(0, 1000)
random.seed(seed)
print("Maze size: " + str(SIZEX) + "x" + str(SIZEY)) 
print("Seed is #" + str(seed))
main()

