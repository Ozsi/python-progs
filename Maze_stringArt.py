import random
import operator
import itertools
# Cretes a random maze
#
# The print formats and the seed generation
# is similar to Kuba's "[Python] Labyrinth generator" programs

# The maze generating algorithm creates a random spanning tree of a
# grid graph using disjoint-set data structure

# Links:
#   Kuba's Labyrinth generator:
#     https://code.sololearn.com/c49Oxtn2gGGQ/#py
#   Spanning tree:
#     https://en.wikipedia.org/wiki/Spanning_tree
#   Disjoint-set data structure:
#     https://en.wikipedia.org/wiki/Disjoint-set_data_structure)

N = (-1, 0)
W = (0, -1)
S = (1, 0)
E = (0, 1)

DIRS = (N, E, S, W)
SIZEX = 20
SIZEY = 15
FIELDS = set(itertools.product(range(SIZEX), range(SIZEY)))
parentMap = {}

#--------------------------------------
# This is the part where the disjoint-set data structure is implemented:
def getParent(node):
  return parentMap.get(node, node)

def setParent(node, parent):
  parentMap[node] = parent

def find(node):
  if getParent(node) == node:
    return node
  else:
    result = find(getParent(node))
    setParent(node, result)
    return result
    
def union(nodeX, nodeY):
  roots = [find(nodeX), find(nodeY)]
  if roots[0] != roots[1]:
    random.shuffle(roots)
    setParent(roots[0], roots[1])

def isAlreadyConnected(nodeX, nodeY):
  return find(nodeX) == find(nodeY)

#---------------------------------------
# Some functions that are required to handle the graph

def addTuples(v1, v2):
  return tuple(map(operator.add, v1, v2))

def createEdge(v1, v2):
  return (v1, v2) if v1 <= v2 else (v2, v1)

def getAllEdges():
  return {edge for edges in map(getIncidentalEdges, FIELDS) for edge in edges}
        
def getIncidentalEdges(field):
  neighbors = getNeighbors(field)
  return map(lambda neighbor: createEdge(neighbor, field), neighbors)

def isInside(field):
  return field in FIELDS

def getNeighbors(field):
  return filter(isInside, map(lambda direction: addTuples(field, direction), DIRS))

def isNodeConnectedTo(node, direction, edges):
  neighbor = addTuples(node, direction)
  edge = createEdge(node, neighbor)
  return edge in edges
  
# Methods for finding the path
def addEdgeToDict(edge, dictionary):
  start, end = edge
  if (start in dictionary):
    dictionary[start].append(end)
  else:
    dictionary[start] = [end]
 
def getConnectionMap(edges):
  result = {}
  for edge in edges:
    addEdgeToDict(edge, result)
    addEdgeToDict(edge[::-1], result)
  return result
      
def backTracePath(start, previousFieldMap):
  current = start
  result = []
  while (current != previousFieldMap[current]):
    result.append(current)
    current = previousFieldMap[current]
  result.append(current)
  return result
      
# Find a path from (0, -1) to (SIZEX - 1, SIZEY) and return the list of coordinates that 
# belong to the path
def findPath(mazeEdges):
  start = (0, -1)
  end = (SIZEX - 1, SIZEY)
  previousFieldMap = {end: end}
  connectionMap = getConnectionMap(mazeEdges)
  queue = [end]
  while len(queue) > 0:
    current = queue.pop(0)
    neighbors = connectionMap[current]
    unvisitedNeighbors = list(filter(lambda neighbor: neighbor not in previousFieldMap, neighbors))
    for neighbor in unvisitedNeighbors:
      previousFieldMap[neighbor] = current
    queue.extend(unvisitedNeighbors)
  if (start in previousFieldMap):
    return backTracePath(start, previousFieldMap)
  else:
    return None

#----------------------------------------
# stringArts


TRANSPARENT = ""

def spaceToTransparent(char):
  return TRANSPARENT if char == " " else char

def createEmpty():
  """
    Creates an empty StringArt
  """
  return lambda coord: TRANSPARENT

def create(string):
  """
    Creates a StringArt from a string
  """
  lines = string.split("\n")
  def result(coord):
    x, y = coord
    if x >= 0  and x < len(lines):
      if y >= 0 and y < len(lines[x]):
        return spaceToTransparent(lines[x][y])
    return TRANSPARENT  
  return result
    
def translate(stringArt, vect):
  """
    Translates a StringArt to a different position
  """
  return lambda coord: stringArt((coord[0] - vect[0], coord[1] - vect[1]))

def transpose(stringArt):
  """
     Transposes a StringArt by switching x and y coordinates
  """
  return lambda coord: stringArt(coord[::-1])

def stringArtUnion(stringArts):
  """
    Takes a list of StringArts and returns a stringArt that
    returns the first non-transparent value
  """
  def result(coord):
    x, y = coord
    for art in stringArts:
      if art(coord) != TRANSPARENT:
        return art(coord)
    return TRANSPARENT
  return result
  
def compose(stringArt, func):
  """
    Applies a string->string function to a stringArt.
  """
  return lambda coord: func(stringArt(coord))

def frame(art, dimension):
  """
    Creates a frame around a StringArt of size sizeX x sizeY
    Example:
     art = abcd111
           efgh222
           ijkl333
           mnop444
     then frameArt(art, 4, 4) =
           +--+
           |ab|
           |ef|
           +--+
  """
  sizeX, sizeY = dimension
  xRange = range(sizeX)
  yRange = range(sizeY)
  xInside = range(1, sizeX - 1)
  yInside = range(1, sizeY - 1)
  def result(coord):
    x, y = coord
    if (x not in xRange) or (y not in yRange):
      return TRANSPARENT
    if (x not in xInside) and (y not in yInside):
      return "+"
    if x not in xInside:
      return "-"
    if y not in yInside:
      return "|"
    return translate(art, (1, 1))(coord)
  return result



#-------------------------------------
# Methods that are responsible for displaying StringArts

def resolveChar(char):
  """
    Resolves a character from a StringArt into a printable format
  """
  return  " " if char == TRANSPARENT else char

def printArt(stringArt, dimension):
  """
    Prints a StringArt in a rectangle of sizeX x sizeY
  """
  sizeX, sizeY = dimension
  for i in range(sizeX):
    line = "".join([resolveChar(stringArt((i, j))) for j in range(sizeY)])
    print(line)


#----------------------------------------
# This part generates the maze itself

def generateMazeEdges(edgeList):
  entranceEdge = ((0, -1), (0, 0))
  exitEdge = ((SIZEX - 1, SIZEY - 1), (SIZEX - 1, SIZEY))
  mazeEdges = {entranceEdge, exitEdge}
  for edge in edgeList:
    node1, node2 = edge
    if not isAlreadyConnected(node1, node2):
      union(node1, node2)
      mazeEdges |= {edge}
  return mazeEdges


#--------------------------------------
# Methods that are responsible for printing

def getMazeSymbol(row, column, edges):
  isField = row % 2 == 1 and column % 2 == 1
  isVertWall = row % 2 == 0 and column % 2 == 1
  isHorWall = row % 2 == 1 and column % 2 == 0
  isInside = row % 2 == 0 and column % 2 == 0
  EMPTY = TRANSPARENT
  OCCUPIED = "#"
  if isField:
    return EMPTY
  if isVertWall:
    leftNeighbor = (row // 2 - 1, column // 2)
    rightNeighbor = (row // 2, column // 2)
    if createEdge(leftNeighbor, rightNeighbor) in edges:
      return EMPTY
    return OCCUPIED
  if isHorWall:
    upNeighbor = (row // 2, column // 2 - 1)
    downNeighbor = (row // 2, column // 2)
    if createEdge(upNeighbor, downNeighbor) in edges:
      return EMPTY
    return OCCUPIED
  return OCCUPIED

def createFieldArt(isNeighbor):
  baseArt = create("# #\n   \n# #")
  wallArt = create("#")
  emptyArt = createEmpty()
  westWallArt = translate(wallArt, (1, 0)) if not isNeighbor["west"] else emptyArt
  northWallArt = translate(wallArt, (0, 1)) if not isNeighbor["north"] else emptyArt
  eastWallArt = translate(wallArt, (1, 2)) if not isNeighbor["east"] else emptyArt
  southWallArt = translate(wallArt, (2, 1)) if not isNeighbor["south"] else emptyArt
  return stringArtUnion([baseArt, westWallArt, northWallArt, eastWallArt, southWallArt])

def createMazeArt(edges):
  def result(coord):
    x, y = coord
    field = (x // 2, y // 2)
    offset = (x % 2, y % 2)
    neighborsMap = {
      "north": isNodeConnectedTo(field, N, edges),
      "east": isNodeConnectedTo(field, E, edges),
      "south": isNodeConnectedTo(field, S, edges),
      "west": isNodeConnectedTo(field, W, edges)
    }
    return createFieldArt(neighborsMap)(offset)
  return result

def createPathArtForField(pathWays):
  def result(coord):
    baseArt = translate(create(".", (1, 1)))
    westWay = 
  return result

def createPathArt(field, path):
  emptyArt = createEmpty()
  if field not in path:
    return emptyArt
  else
      
  return result


def printMaze(sizeX, sizeY, mazeEdges, path):
  art = stringArtUnion([createMazeArt(mazeEdges), createPathArt(path)])
  printArt(art, (2 * sizeX + 1, 2 * sizeY + 1))


#------------------------------------
# The main method

def main():
  EDGESET = getAllEdges()
  edgeList = list(EDGESET)
  random.shuffle(edgeList)
  mazeEdges = generateMazeEdges(edgeList)
  path = findPath(mazeEdges)
  printMaze(SIZEX, SIZEY, mazeEdges, path)

  

seed = random.randint(0, 1000)
random.seed(seed)
print("Maze size: " + str(SIZEX) + "x" + str(SIZEY)) 
print("Seed is #" + str(seed))
print("\"I will instruct you and teach you in the way you should go; I will counsel you with my loving eye on you.\" (Psalms 32, 8)\n")
main()

