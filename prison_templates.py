# Examples for the Prison Escape challenge
# Feel free to use the Prison class to get started.

WALL = "#"
EXIT = "E"
START = "P"
FLOOR = " "

GEM_CHARACTERS = set(map(str, range(10)))
DOOR_CHARACTERS = set(c for c in "abcdefghij")
WALL_CHARACTERS = {WALL}
EMPTY_CHARACTERS = {START, FLOOR, EXIT}

class Prison:
    def __init__(self, string):
        self.__string = string
        self.__lines = string.split("\n")
        self.__positions = self.__getPositions(string)

    def print(self):
        print(self.__string)
        
    def getCharAt(self, coord):
        if coord not in self.__positions:
            return WALL
        row, column = coord
        return self.__lines[row][column]

    def isGem(self, coord):
        return self.getCharAt(coord) in GEM_CHARACTERS

    def isDoor(self, coord):
        return self.getCharAt(coord) in DOOR_CHARACTERS

    def isEmpty(self, coord):
        return self.getCharAt(coord) in EMPTY_CHARACTERS

    def isWall(self, coord):
        return self.getCharAt(coord) in WALL_CHARACTERS

    def __getPositions(self, string):
        x = 0
        y = 0
        result = set()
        for character in string:
            if character == "\n":
                x += 1
                y = 0
            else:
                result.add((x, y))
                y += 1
        return result

    def findPositionsOf(self, char):
        for coord in self.__positions:
            if self.getCharAt(coord) == char:
                yield coord
                
    def findFirstPosition(self, char):
        for coord in self.findPositionsOf(char):
            return coord
        return None
    
    def findExit(self):
        return self.findFirstPosition(EXIT)

    def findStart(self):
        return self.findFirstPosition(START)

    def getAccessibleNeighbors(self, coord, hasGem = False):
        offsets = ((0, 1), (1, 0), (0, -1), (-1, 0))
        def add(*vectors):
            return tuple(map(sum, zip(*vectors)))
        
        neighbors = set(add(coord, offset) for offset in offsets)
        def isAccessible(coord):
            return self.isEmpty(coord) or self.isGem(coord) or\
                (self.isDoor(coord) and hasGem)
        accessibleNeighbors = set(filter(isAccessible, neighbors))        
        return accessibleNeighbors

trivial = Prison("""\
#######
#P   E#
#######\
""")
    
impossible = Prison("""\
#######
#P#   #
# #a#0#
#   #E#
#######
""")

doorless = Prison("""\
#########
#P#     #
# # # # #
# # ### #
#     #E#
#########\
""")

one_door = Prison("""\
#######
#0 PaE#
#######\
""")

multidoor = Prison("""\
#########
#0  c  1#
### ### #
# P     #
# #####b#
# #   #a#
#2  # #E#
#########\
""")


print("The code contains an implementation of\
 the Prison class to help you get started.\n")
print("Some examples:")
trivial.print()
print()
one_door.print()
print()
doorless.print()
print()
multidoor.print()
print()
impossible.print()
