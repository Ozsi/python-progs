import random

def atLeast(amount):
    result = lambda x: False
    result.gold = lambda player: player.gold >= amount 
    return result

def has(item):
    def result(player):
        return player.hasItem(item)
    result.gold = lambda player: player.gold >= item 
    return result

def passTest(player, requirements):
    return all(requirement(player) for requirement in requirements)

def gold(amount):
    return amount

class Player:
    def __init__(self):
        self.items = set()
        self.gold = 0

    def passes(self, requirements):
        return passTest(self, requirements)

    def hasItem(self, item):
        return item in self.items

    def giveItem(self, item):
        if isinstance(item, int):
            self.gold += item
        else:
            self.items.add(item)

    def takeItem(self, item):
        if isinstance(item, int):
            self.gold -= item
        else:
            self.items.remove(item)

    def __str__(self):
        strings = "Items:\n"
        itemsStr = "\n".join(self.items)
        return strings + itemsStr + "\nGold: " + str(self.gold)

    def getDescription(self):
        strings = "You have the following items: "
        itemsStr = ", ".join(filter(lambda x: not x.startswith("_"),self.items))
        return strings + itemsStr + " and %s gold." % self.gold

def doEvent(player, event):
    if "text" in event:
        print(event["text"])
    for item in event.get("gives", []):
        player.giveItem(item)
    for item in event.get("takes", []):
        player.takeItem(item)

def attemptEvent(player, event):
    if player.passes(event.get("requirements", [])):
        chance = event.get("chance", 100)
        if random.randrange(0, 100) < chance:
            doEvent(player, event)
events = [
    {
        "chance": 100,
        "requirements": [],
        "text": "You start your journey.",
        "gives": [gold(10), "sword", "_moodNeutral"],
        "takes": []
    },
    {
        "chance": 50,
        "gives": ["_moodHappy"],
        "takes": ["_moodNeutral"]
    },
    {
        "chance": 100,
        "requirements": [has("sword")],
        "text": "You find a treasure chest. You are able to \
force it open using your sword. You find a shield inside.",
        "gives": ["shield"]
},
    {
        "chance": 75,
        "requirements": [has(10).gold],
        "text": "You pay 10 gold.",
        "takes": [gold(10)]
    }
]

player = Player()

def main():
    random.seed()
    for event in events:
        attemptEvent(player, event)
        print(player.getDescription())
        print()
        
main()

