START = "start"
DOWNLOAD = "download"
BUILD_TREE = "build"
HALT = "halt"
FINISH = "finish"

class Machine:
    def __init__(self, tran):
        self.tran = tran
        self.state = START

    def accept(self, line):
        self.state = self.tran(self.state, line)

class Node:
    def __init__(self, label):
        self.label = label
        self.children = []
        self.depth = 0

    def addChild(self, node):
        self.children.append(node)
        node.parent = self
        node.depth = self.depth + 1

def transFunc(state, line):
    if state == START:
        if line.startswith("[INFO] --- maven-dependency-plugin"):
            return DOWNLOAD
        else:
            return START
    if state == DOWNLOAD:
        if line.startswith("["):
            return BUILD_TREE
        else:
            return DOWNLOAD
    if state == BUILD_TREE or state == HALT:
        if line.startswith("[INFO] BUILD"):
            return FINISH
        if ":" not in line:
            return HALT
        return BUILD_TREE
    return FINISH

def stripLogLevel(line):
    return line.split("] ")[1]

def getTreeDepth(line):
    dashindex = line.find("- ")
    if dashindex == -1:
        return 1
    else:
        return (dashindex // 3) + 2
    
def getNodeLabel(line):
    return line.split(":")[1]

def growTree(current, line):
    line = stripLogLevel(line)
    depth = getTreeDepth(line)
    parent = current
    while depth <= parent.depth:
        parent = parent.parent
    current = Node(getNodeLabel(line))
    parent.addChild(current)
    return current

def createTree(file):
    root = Node("root")
    current = root
    for line in file:
        machine.accept(line)
        if machine.state == BUILD_TREE:
            current = growTree(current, line)
    return root

def printNode(node, depth):
    print((" "*depth) + node.label)
    for child in node.children:
        printNode(child, depth + 1)

if __name__ == "__main__":
    machine = Machine(transFunc)
    with open("C:/1Egyéb/deptree.txt") as file:
        root = createTree(file)
        printNode(root, 0)
