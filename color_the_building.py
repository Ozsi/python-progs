RED_FLOOR       = "*  Red  *"
BLUE_FLOOR      = "* Blue  *"
FLOOR_SEPARATOR = "*********"

FLOOR_DICTIONARY = {
    'b': BLUE_FLOOR,
    'r': RED_FLOOR
}

def appendBlueFloor(coloring):
    return coloring + "b"

def appendBlueAndRedFloor(coloring):
    return coloring + "br"

def getColorings(floors):
    if floors <= 0:
        yield ""
    if floors == 1:
        yield from ["b", "r"]
    if floors > 1:
        yield from map(appendBlueFloor, getColorings(floors - 1))
        yield from map(appendBlueAndRedFloor, getColorings(floors - 2))

def printColoring(coloring):
    if len(coloring) < 1:
        return
    for color in coloring:
        print(FLOOR_SEPARATOR)
        print(FLOOR_DICTIONARY[color])
    print(FLOOR_SEPARATOR)

def toReadable(coloring):
    colorDictionary = {
        'r': "Red",
        'b': "Blue"
    }
    return ", ".join(colorDictionary[color] for color in coloring)

def main():
    try:
        floors = int(input())
    except:
        floors = 0

    counter = 0
    for coloring in getColorings(floors):
        print(toReadable(coloring))
        #printColoring(coloring)
        print()
        counter +=1
    print("There are %s good colorings." % counter)

main()
