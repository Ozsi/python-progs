def getFibonacciNumbersUpTo(n):
    result = [1, 1]
    while result[-1] <= n:
        result.append(result[-1] + result[-2])
    return result[1:-1]

def convertToFibonacci(n):
    fib = getFibonacciNumbersUpTo(n)[::-1]
    result = []
    for i in fib:
        if n >= i:
            n -= i
            result.append(1)
        else:
            result.append(0)
    return result[::-1]

def getExplanation(n):
    fib = getFibonacciNumbersUpTo(n)
    digits = convertToFibonacci(n)
    result = str(n) + " = "
    result += " + ".join(map(lambda x: "*".join(map(str, x)), zip(digits, fib)))
    return result

