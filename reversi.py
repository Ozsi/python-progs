# A multiplayer game of reversi
# See rules here:
# https://en.wikipedia.org/wiki/Reversi

# How to play:
# A) START A NEW GAME
# To start a new game enter the following when prompted:
# start
# <your name>
#
# You will get a bunch of random letters - copy them and send them
# to your opponent. This string is called game state

# B) ACCEPTING A CHALLENGE
# If you get challenged, your opponent will send you a string
# Start the program and enter the following:
# <The string your challenger sent you aka. game state>
# <your name>
#
# You will get another bunch of random letters - send these back to
# your opponent to start the game for real

# C) All subsequent moves
# After you get the new game state, run the program and enter:
# <game state>
#
# You will see the board printed twice.
# Plan your move, restart the program and enter:
# <the same game state again>
# <your move ie. C3>
#
# You will get a new game state after this. Check if it's their turn.
# If yes, send it to them. If it's still your turn, then move again.
# Repeat until finished or get bored
#
# Disclaimer:
# I have not tested this program thoroughly, so bugs can 
# and most probably will happen :)

#-----------------------
from itertools import product
import base64
import json
import string

TRANSPARENT = ""

def spaceToTransparent(char):
  return TRANSPARENT if char == " " else char

def createEmpty():
  """
    Creates an empty StringArt
  """
  return lambda coord: TRANSPARENT

def create(string):
  """
    Creates a StringArt from a string
  """
  lines = string.split("\n")
  def result(coord):
    x, y = coord
    if x >= 0  and x < len(lines):
      if y >= 0 and y < len(lines[x]):
        return spaceToTransparent(lines[x][y])
    return TRANSPARENT  
  return result
    
def translate(stringArt, vect):
  """
    Translates a StringArt to a different position
  """
  return lambda coord: stringArt((coord[0] - vect[0], coord[1] - vect[1]))

def transpose(stringArt):
  """
     Transposes a StringArt by switching x and y coordinates
  """
  return lambda coord: stringArt(coord[::-1])

def union(stringArts):
  """
    Takes a list of StringArts and returns a stringArt that
    returns the first non-transparent value
  """
  def result(coord):
    x, y = coord
    for art in stringArts:
      if art(coord) != TRANSPARENT:
        return art(coord)
    return TRANSPARENT
  return result
  
def compose(stringArt, func):
  """
    Applies a string->string function to a stringArt.
  """
  return lambda coord: func(stringArt(coord))

def frame(art, dimension):
  """
    Creates a frame around a StringArt of size sizeX x sizeY
    Example:
     art = abcd111
           efgh222
           ijkl333
           mnop444
     then frameArt(art, 4, 4) =
           +--+
           |ab|
           |ef|
           +--+
  """
  sizeX, sizeY = dimension
  xRange = range(sizeX)
  yRange = range(sizeY)
  xInside = range(1, sizeX - 1)
  yInside = range(1, sizeY - 1)
  translatedArt = translate(art, (1, 1))
  def result(coord):
    x, y = coord
    if (x not in xRange) or (y not in yRange):
      return TRANSPARENT
    if (x not in xInside) and (y not in yInside):
      return "+"
    if x not in xInside:
      return "-"
    if y not in yInside:
      return "|"
    return translatedArt(coord)
  return result

def resolveChar(char):
  """
    Resolves a character from a StringArt into a printable format
  """
  return  " " if char == TRANSPARENT else char

def printArt(stringArt, dimension):
  """
    Prints a StringArt in a rectangle of sizeX x sizeY
  """
  sizeX, sizeY = dimension
  for i in range(sizeX):
    line = "".join([resolveChar(stringArt((i, j))) for j in range(sizeY)])
    print(line)
    
#------------------------

X = "X"
O = "O"
EMPTY = " "
SIZE = 8
FIELDS = tuple(product(range(SIZE), range(SIZE)))
STA = "        ,        ,        ,   OX   ,   XO   ,        ,        ,        "


def getPositionFrom(pos, direction, dist):
  i, j = pos
  di, dj = direction
  return (i + dist * di, j + dist * dj)

def isOpponentFrom(table, pos, direction, dist):
  return table(getPositionFrom(pos, direction, dist)) == O

def isMineFrom(table, pos, direction, dist):
  return table(getPositionFrom(pos, direction, dist)) == X

directions = ((-1, 0), (-1, 1), (0, 1), (1, 1),
                (1, 0), (1, -1), (0, -1), (-1, -1))
def getPositionsToSwitch(table, stepPos):
  result = []
  for direction in directions:
    dist = 1
    switchCandidates = []
    while isOpponentFrom(table, stepPos, direction, dist):
      switchCandidates.append(getPositionFrom(stepPos, direction, dist))
      dist += 1
    if isMineFrom(table, stepPos, direction, dist):
      result += switchCandidates
  return result

def isValidMove(table, step):
  return len(getPositionsToSwitch(table, step)) > 0 

def getValidMoves(table):
  return tuple(field for field in FIELDS if isValidMove(table, field))

def getValidXMoves(table):
  return getValidMoves(table)

def getValidOMoves(table):
  return getValidMoves(reverseTable(table))

def doMove(table, stepPos):
  """
    Return a table with the move from player X done on pos.
  """
  posToSwitch = getPositionsToSwitch(table, stepPos)
  def result(pos):
    if pos == stepPos or pos in posToSwitch:
        return X
    return table(pos)
  return result

def doXMove(table, stepPos):
  return doMove(table, stepPos)

def doOMove(table, stepPos):
  return reverseTable(doMove(reverseTable(table), stepPos))

def canMove(table):
  return len(getValidMoves(table)) > 0

def canXMove(table):
  return canMove(table)

def canOMove(table):
  return canMove(reverseTable(table))

def getNormalizedTable(gameState):
  table = getTable(gameState)
  if getNextPlayer(gameState) == 1:
    table = reverseTable(table)
  return table

def isValidMoveGameState(gameState, pos):
  table = getNormalizedTable(gameState)
  return isTablePos(pos) and isValidMove(table, pos)


def countSymbols(table):
  result = {X: 0, O: 0, EMPTY: 0}
  for pos in FIELDS:
    result[table(pos)] += 1
  return result

def isGameOver(table):
  symbolStat = countSymbols(table)
  return min(symbolStat[X], symbolStat[O], symbolStat[EMPTY]) == 0

def createEmptyTable():
  return lambda pos: EMPTY

reverseDict = {
      X: O,
      O: X,
      EMPTY: EMPTY
}
def reverseTable(table):
  return lambda pos: reverseDict.get(table(pos), EMPTY)

def isTablePos(pos):
  i, j = pos
  return i >= 0 and i < SIZE and j >= 0 and j < SIZE

def createTableFrom(string):
  rows = string.split(",")
  def result(pos):
    i, j = pos
    if i < 0 or i >= len(rows):
      return EMPTY
    if j < 0 or j >= len(rows[i]):
      return EMPTY
    return rows[i][j]
  return result

def getRowIndexDisplay(size):
  artString = "".join(map(lambda x: str(x+1), range(size)))  
  return transpose(create(artString))

def getColumnIndexDisplay(size):
  alphabet = string.ascii_uppercase
  artString = alphabet[:size]  
  return create(artString)

def createTableDisplay(table):
  framedTable = translate(frame(table, (SIZE + 2, SIZE + 2)), (1, 1))
  rowIndices = translate(getRowIndexDisplay(SIZE), (2, 0))
  columnIndices = translate(getColumnIndexDisplay(SIZE), (0, 2))
  return union((framedTable, rowIndices, columnIndices))

#----------------------------------

def getNextPlayerName(gameState):
  player0 = getPlayer0(gameState)
  player1 = getPlayer1(gameState)
  return player0 if getNextPlayer(gameState) == 0 else player1

def createScoreTable(gameState):
  table = getTable(gameState)
  stats = countSymbols(table)
  name0 = getPlayer0(gameState)
  name1 = getPlayer1(gameState)
  
  resultTemplate = "%s - X: %s\n%s - O: %s\nSteps left: %s\n"
  resultString = resultTemplate % (name0, stats[X], name1, stats[O], stats[EMPTY])
  resultString += "\nIt's %s's turn" % getNextPlayerName(gameState)
  return frame(create(resultString), (SIZE + 2, 26))

def getTableStringRow(table, row):
  return "".join([table((row, column)) for column in range(SIZE)])

def dumpTableToString(table):
  return ",".join([getTableStringRow(table, row) for row in range(SIZE)])

def encodeString(string):
    return bytes(string, "UTF-8")

def encodeDict(dictionary):
  string = json.dumps(dictionary)
  return base64.b64encode(encodeString(string)).decode("UTF-8")

def decodeDict(string):
   decodedString = base64.b64decode(encodeString(string)).decode("UTF-8")
   return json.loads(decodedString)

def decodeGameState(string):
  rawGameState = decodeDict(string)
  result = dict(rawGameState)
  result["table"] = createTableFrom(getTableString(rawGameState))
  return result

def encodeGameState(gameState):
  rawGameState = dict(gameState)
  rawGameState["table"] = dumpTableToString(getTable(gameState))
  return encodeDict(rawGameState)

def printGameState(gameState):
  if gameState == None:
    return
  table = getTable(gameState)
  displayedTable = createTableDisplay(table)
  scoreTable = translate(createScoreTable(gameState), (1, SIZE + 2))
  art = union([displayedTable, scoreTable])
  printArt(art, (SIZE + 3, 36))

def createStartingGameState(playerName):
  return {"Player0": playerName}

def populatePlayer1Name(gameState, name):
  clone = dict(gameState)
  clone["Player1"] = name
  clone["nextPlayer"] = 0
  return clone

START_TABLE = createTableFrom(STA)
def getTable(gameState):
  return gameState.get("table", START_TABLE)

def getTableString(rawGameState):
  return rawGameState.get("table", STA)

def getPlayer0(gameState):
  return gameState.get("Player0", None)

def getPlayer1(gameState):
  return gameState.get("Player1", None)

def isPlayer1NameAvailable(gameState):
  return "Player1" in gameState

def getNextPlayer(gameState):
  return gameState.get("nextPlayer", None)

def doStart():
  print("Starting game, please enter your name: ")
  name = getInput()
  gameState = createStartingGameState(name)
  displayFinishingStep(gameState)

def doSecondStep(gameState):
  print("Please enter your name: ")
  player1Name = getInput()
  gameStateWithPlayerNames = populatePlayer1Name(gameState, player1Name)
  return gameStateWithPlayerNames

def getPosFromStep(step):
  column = string.ascii_uppercase.index(step[0].upper())
  row = int(step[1]) - 1
  return (row, column)

moveDeciderDict = {
  0: canXMove,
  1: canOMove
}
def canOtherPlayerMove(table, currentPlayer):
  moveDecider = moveDeciderDict[1 - currentPlayer]
  return moveDecider(table)

moveFunctionDict = {
  0: doXMove,
  1: doOMove
}
def doStepOnGameState(gameState, pos):
  result = dict(gameState)
  table = getTable(gameState)
  currentPlayer = getNextPlayer(gameState)
  moveFunction = moveFunctionDict[currentPlayer]
  result["table"] = moveFunction(table, pos)
  if canOtherPlayerMove(table, currentPlayer):
    result["nextPlayer"] = 1 - currentPlayer
  return result

def doPlayerStep(gameState):
  print("Current state of the board:")
  printGameState(gameState)
  print("Enter the field you want to check (ie. C1): ")
  step = getInput()
  if step == "":
    return gameState
  pos = getPosFromStep(step)
  if isValidMoveGameState(gameState, pos):
    newGameState = doStepOnGameState(gameState, pos)
  else:
    print("That's not a valid move, try again")
    newGameState = None
  return newGameState 

def getWinnerName(gameState):
  table = getTable(gameState)
  symbols = countSymbols(table)
  if symbols[X] > symbols[O]:
    return getPlayer0(gameState)
  if symbols[O] > symbols[X]:
    return getPlayer1(gameState)
  return "No one"

def displayFinishingStep(gameState):
  if gameState == None:
    return
  table = getTable(gameState)
  if isGameOver(table):
    winner = getWinnerName(gameState, table)
    print("%s won!" % winner)
  print("\nUse this string as a game status to continue from here:")
  print(encodeGameState(gameState))

def doStep(encodedGameState):
  gameState = decodeGameState(encodedGameState)
  if not isPlayer1NameAvailable(gameState):
    newGameState = doSecondStep(gameState)
  else:
    newGameState = doPlayerStep(gameState)
  printGameState(newGameState)
  displayFinishingStep(newGameState)

def getInput():
  try:
    result = input()
  except EOFError:
    result = ""
  print("Got >%s< as input" % result)
  return result

def main():
  print("Enter a game status or \"start\" to start a new game")
  command = getInput()
  if "start" == command:
    doStart()
  else:
    doStep(command)
print("Behold, I have received commandment to bless: and he hath blessed; and I cannot reverse it. (Numbers 23, 20)")
main()
