TRANSPARENT = ""

def spaceToTransparent(char):
  return TRANSPARENT if char == " " else char

def createEmpty():
  """
    Creates an empty StringArt
  """
  return lambda coord: TRANSPARENT

def createArt(string):
  """
    Creates a StringArt from a string
  """
  lines = string.split("\n")
  def result(coord):
    x, y = coord
    if x >= 0  and x < len(lines):
      if y >= 0 and y < len(lines[x]):
        return spaceToTransparent(lines[x][y])
    return TRANSPARENT  
  return result
    
def translate(stringArt, vect):
  """
    Translates a StringArt to a different position
  """
  return lambda coord: stringArt((coord[0] - vect[0], coord[1] - vect[1]))
  
def compose(stringArt, func):
  """
    Applies a string->string function to a stringArt.
  """
  return lambda coord: func(stringArt(coord))

def union(stringArts):
  """
    Takes a list of StringArts and returns a stringArt that
    returns the first non-transparent value
  """
  def result(coord):
    for art in stringArts:
      if art(coord) != TRANSPARENT:
        return art(coord)
    return TRANSPARENT
  return result

def createFrame(art, dimension):
  """
    Creates a frame around a StringArt of size sizeX x sizeY
    Example:
     art = abcd111
           efgh222
           ijkl333
           mnop444
     then frameArt(art, 4, 4) =
           +--+
           |ab|
           |ef|
           +--+
  """
  sizeX, sizeY = dimension
  xRange = range(sizeX)
  yRange = range(sizeY)
  xInside = range(1, sizeX - 1)
  yInside = range(1, sizeY - 1)
  translatedArt = translate(art, (1, 1))
  def result(coord):
    x, y = coord
    if (x not in xRange) or (y not in yRange):
      return TRANSPARENT
    if (x not in xInside) and (y not in yInside):
      return "+"
    if x not in xInside:
      return "-"
    if y not in yInside:
      return "|"
    return translatedArt(coord)
  return result

def resolveChar(char):
  """
    Resolves a character from a StringArt into a printable format
  """
  return  " " if char == TRANSPARENT else char

def printArt(stringArt, dimension):
  """
    Prints a StringArt in a rectangle of sizeX x sizeY
  """
  sizeX, sizeY = dimension
  for i in range(sizeX):
    line = "".join([resolveChar(stringArt((i, j))) for j in range(sizeY)])
    print(line)

#--------------------------------

men = createArt(" O       O\n<|\\     /|>\n |       |\n/ \\     / \\")
brick = createArt(" ___\n|___|")

def menWithBrick(x):
  return union([menWithoutBrick(x), translate(brick, (0, x + 3))])

def menWithoutBrick(x):
  return translate(men, (0, x))

def createWallArt(brickCoordList):
  artList = [translate(brick, brickCoord) for brickCoord in brickCoordList]
  return union(artList)
  
#printArt(scene, (4, 21))

wallArt = translate(createWallArt([(0, 0), (0, 4), (0, 8), (0, 12)]), (2, 0))

for frame in range(18):
  content = union([menWithBrick(frame - 10), wallArt])
  scene = createFrame(content, (6, 30))
  printArt(scene, (6, 30))

wallArt = translate(createWallArt([(-1, 10), (0, 0), (0, 4), (0, 8), (0, 12)]), (2, 0))

for frame in range(18):
  content = union([menWithoutBrick(7 - frame), wallArt])
  scene = createFrame(content, (6, 30))
  printArt(scene, (6, 30))
