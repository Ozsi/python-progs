from PIL import Image
from itertools import product

LETTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'\"+!%/\\=(),?.:-_1234567890 "

PIXELS = tuple(product(range(15), range(22)))

im = Image.open('C:/1Egyéb/letters.bmp')

def getImageByIndex(n):
    return im.crop((n * 16, 0, (n+1)*16 - 1, 22))

def createImages():
    return {LETTERS[n]: getImageByIndex(n) for n in range(len(LETTERS))}

imageByLetter = createImages()

def diffImages(im1, im2):
    diff = 0
    for pixel in PIXELS:
        diff += max(im1.getpixel(pixel) - im2.getpixel(pixel), 0) // 255
    return diff

def diffLetters(letter1, letter2):
    return diffImages(imageByLetter[letter1], imageByLetter[letter2])

def getDifferences(letter):
    return {other: diffLetters(letter, other) for other in LETTERS}

def getDifferenceMap():
    return {letter: getDifferences(letter) for letter in LETTERS}

def getDistribution(diffs):    
    return {char: 20 - diffs[char] for char in diffs if diffs[char] <= 20}

def getDistributions():
    diffMap = getDifferenceMap()
    return {char: getDistribution(diffMap[char]) for char in diffMap}


