from itertools import product

# Prints the multiplication table for number 0 to 10
# Look for the hidden features in the comments :)

TRANSPARENT = ""

def spaceToTransparent(char):
  return TRANSPARENT if char == " " else char

def createEmpty():
  """
    Creates an empty StringArt
  """
  return lambda coord: TRANSPARENT

def create(string):
  """
    Creates a StringArt from a string
  """
  lines = string.split("\n")
  def result(coord):
    x, y = coord
    if x >= 0  and x < len(lines):
      if y >= 0 and y < len(lines[x]):
        return spaceToTransparent(lines[x][y])
    return TRANSPARENT  
  return result
    
def translate(stringArt, vect):
  """
    Translates a StringArt to a different position
  """
  return lambda coord: stringArt((coord[0] - vect[0], coord[1] - vect[1]))
  
def compose(stringArt, func):
  """
    Applies a string->string function to a stringArt.
  """
  return lambda coord: func(stringArt(coord))

def union(stringArts):
  """
    Takes a list of StringArts and returns a stringArt that
    returns the first non-transparent value
  """
  def result(coord):
    for art in stringArts:
      if art(coord) != TRANSPARENT:
        return art(coord)
    return TRANSPARENT
  return result

#---------------------------
# sizedArts are dictionaries that contain a size and a
# stringArt.

def createSizedArt(size, art):
  """
    Creates a sized art
  """
  return {"size": size, "art": art}

def getArt(sizedArt):
  """
    Returns the art of a sizedArt
  """
  return sizedArt["art"]

def getSize(sizedArt):
  """
    Returns the size of a SizedArt
  """
  return sizedArt["size"]

def getWidth(sizedArt):
  """
    Returns the width of a sizedArt
  """
  return getSize(sizedArt)[1]

def getHeight(sizedArt):
  """
    Returns the height of a sizedArt
  """
  return getSize(sizedArt)[0]

def resize(sizedArt, size):
  """
    Resizes a sized art
  """
  return createSizedArt(size, getArt(sizedArt))

def createEmptySizedArt():
  """
    Creates an empty sized art
  """
  return createSizedArt((0, 0), createEmpty())

#-----------------------------
# These methods are used for creating the table

def applyByCoordinates(functions, values):
  f, g = functions
  x, y = values
  return (f(x), g(y))

def applyToEach(f, values):
  x, y = values
  return (f(x), f(y))

def getMaxStat(fieldList, stat):
  return max(stat(field) for field in fieldList)

def getRow(fields, row, columns):
  empty = createEmptySizedArt()
  return [fields.get((row, j), empty) for j in range(columns)]

def getColumn(fields, column, rows):
  empty = createEmptySizedArt()
  return [fields.get((i, column), empty) for i in range(rows)]

def getRowHeight(fields, row, columns):
  return getMaxStat(getRow(fields, row, columns), getHeight)

def getColumnWidth(fields, column, rows):
  return getMaxStat(getColumn(fields, column, rows), getWidth)

def getCellWidthsAndHeights(dimension, fields):
  rows, columns = dimension  
  heights = [getRowHeight(fields, i, columns) for i in range(rows)]
  widths = [getColumnWidth(fields, j, rows) for j in range(columns)]
  return (widths, heights)

def getSeparators(rows, heights):
  return {sum(heights[:i]) + i for i in range(rows + 1)}

def getXYSeparators(dimension, fields):
  rows, columns = dimension
  widths, heights  = getCellWidthsAndHeights(dimension, fields)
  xSeparators = getSeparators(rows, heights)
  ySeparators = getSeparators(columns, widths)
  return (xSeparators, ySeparators)

def getFieldPositions(dimension):
  rows, columns = dimension
  return product(range(rows), range(columns))

def getMatchingIndex(x, xSeparators):
  return len(list(filter(lambda n: n <= x, xSeparators))) - 1

def getMatchingIndexGetter(xSeparators):
  def result(x):
    return getMatchingIndex(x, xSeparators)
  return result

def getMatchingIndexGetters(xySeparators):
  return applyToEach(getMatchingIndexGetter, xySeparators)

def getFieldSelector(xySeparators):
  matchingIndexGetters = getMatchingIndexGetters(xySeparators)
  def result(coord):
    return applyByCoordinates(matchingIndexGetters, coord)
  return result

def getOffset(x, xSeparators):
  return x - max(filter(lambda n: n <= x, xSeparators), default = -1) - 1

def getOffsetGetter(xSeparators):
  def result(x):
    return getOffset(x, xSeparators)
  return result

def getOffsetGetters(xySeparators):
  return applyToEach(getOffsetGetter, xySeparators)

def getOffsetCalculator(xySeparators):
  offsetGetters = getOffsetGetters(xySeparators)
  def result(coord):
    return applyByCoordinates(offsetGetters, coord)
  return result

def getFrameSize(xySeparators):
  return applyToEach(lambda x: max(x) + 1, xySeparators)

def getSeparatorCoordDecider(separators):
  separatorSet = set(separators)
  def result(x):
    return x in separatorSet
  return result

def getSeparatorCoordDeciders(xySeparators):
  return applyToEach(getSeparatorCoordDecider, xySeparators)

def getSeparatorStatusGetter(xySeparators):
  separatorDeciders = getSeparatorCoordDeciders(xySeparators)
  def result(coord):
    return applyByCoordinates(separatorDeciders, coord)
  return result

characterBySeparatorStatus = {
    #isXSeparator, isYSeparator
    (True,         True):  "+",
    (True,         False): "-",
    (False,        True):  "|",
    (False,        False): TRANSPARENT
}

def createTableFrame(separatorStatusGetter):
  def result(coord):
    separatorStatus = separatorStatusGetter(coord)
    return characterBySeparatorStatus[separatorStatus]
  return result

def getTableFrame(xySeparators):
  separatorStatusGetter = getSeparatorStatusGetter(xySeparators)
  resultArt = createTableFrame(separatorStatusGetter)
  resultSize = getFrameSize(xySeparators)
  return createSizedArt(resultSize, resultArt)

def createArtSelector(fields, fieldSelector, default):
  def result(coord):
    selectedPos = fieldSelector(coord)
    return fields.get(selectedPos, default)
  return result

def getArtSelector(fields, xySeparators):
  fieldSelector = getFieldSelector(xySeparators)
  empty = createEmptySizedArt()
  return createArtSelector(fields, fieldSelector, empty)

def createTableContentArt(artSelector, offsetCalculator):
  def result(coord):
    selectedSizedArt = artSelector(coord)
    offset = offsetCalculator(coord)
    return getArt(selectedSizedArt)(offset)
  return result

def getTableContentArt(fields, xySeparators):
  artSelector = getArtSelector(fields, xySeparators)
  offsetCalculator = getOffsetCalculator(xySeparators)
  return createTableContentArt(artSelector, offsetCalculator)

def createTable(dimension, fields):
  """
    Creates a table of sizedArts from a dictionary
  """
  xySeparators = getXYSeparators(dimension, fields)
  
  tableFrame = getTableFrame(xySeparators)
  tableContentArt = getTableContentArt(fields, xySeparators)
  
  resultArt = union([getArt(tableFrame), tableContentArt])
  resultSize = getSize(tableFrame)
  return createSizedArt(resultSize, resultArt)


#-------------------------------------
# Methods that are responsible for displaying stringArts and sizedArts

def resolveChar(char):
  """
    Resolves a character from a StringArt into a printable format
  """
  return " " if char == TRANSPARENT else char

def printArt(stringArt, dimension):
  """
    Prints a StringArt in a rectangle of sizeX x sizeY
  """
  sizeX, sizeY = dimension
  for i in range(sizeX):
    line = "".join([resolveChar(stringArt((i, j))) for j in range(sizeY)])
    print(line)

def printSizedArt(sizedArt):
  """
    Prints a sized art.
  """
  printArt(getArt(sizedArt), getSize(sizedArt))

#-----------------------------------
# From now on, creating the tables:

def getSizedArtFromProvider(stringProvider, pos):
  artString = stringProvider(pos)
  height = 0 if artString == "" else len(artString.split("\n"))
  size = (height, len(artString))
  return createSizedArt(size, create(artString))

def createTableDict(dimension, stringProvider):
  positions = getFieldPositions(dimension)
  return {pos: getSizedArtFromProvider(stringProvider, pos)
          for pos in positions}

def getOperationTableStringProvider(operation, symbol):
  def result(pos):
    i, j = pos
    if i == 0 and j == 0:
      return symbol
    if i == 1 or j == 1:
      return ""
    if i == 0 or j == 0:
      return str(max(i - 2, j - 2))
    return str(operation(i - 2, j - 2))
  return result

def multiplication(x, y):
  return x * y

def addition(x, y):
  return x + y

def subtraction(x, y):
  return x - y

def exponentiation(x, y):
  return x ** y

def division(x, y):
  return "N/A" if y == 0 else x // y

def printOperation(operation, symbol, size = 10):
  tableSize = (size + 3, size + 3)
  provider = getOperationTableStringProvider(operation, symbol)
  tableDict = createTableDict(tableSize, provider)
  tableArt = createTable(tableSize, tableDict)
  printSizedArt(tableArt)

print("And He took him outside and said, \"Now look" +
      "toward the heavens, and count the stars, " +
      "if you are able to count them.\" " +
      "And He said to him, \"So shall " +
      "your descendants be.\" (Genesis 15, 5)")

# Hidden feature: print other operations
#print("Multiplication:")
printOperation(multiplication, "*")
#print("Addition:")
#printOperation(addition, "+")
#print("Subtraction:")
#printOperation(subtraction, "-")
#print("Division:")
#printOperation(division, "//")

# Be careful with this :)
#print("Exponentiation:")
#printOperation(exponentiation, "**", 7)

